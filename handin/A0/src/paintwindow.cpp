/*Philip Markowski
pmarkows
20378837 */
#include <QtWidgets>
#include <iostream>
#include "paintwindow.hpp"

PaintWindow::PaintWindow() {
    setWindowTitle("488 Paint");

    QVBoxLayout *layout = new QVBoxLayout;

    m_canvas = new PaintCanvas(this);
    layout->addWidget(m_canvas);
    setCentralWidget(new QWidget);
    centralWidget()->setLayout(layout);

    createMenu();
    createButton();

    layout->addWidget(m_button_quit);
}

void PaintWindow::createMenu() {
    // Adding the drop down menu to the menubar
    m_menu_app = menuBar()->addMenu(tr("&Application"));
    m_menu_tools = menuBar()->addMenu(tr("&Tools"));
    m_menu_colour = menuBar()->addMenu(tr("Colour"));
    m_menu_help = menuBar()->addMenu(tr("&Help"));

    // Adding the menu items for each drop down menu
    QAction* quitAct = new QAction(tr("&Quit"), this);
    quitAct->setShortcuts(QKeySequence::Quit);
    quitAct->setStatusTip(tr("Exits the program"));
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));
    m_menu_app->addAction(quitAct);

    QAction* clearAct = new QAction(tr("&Clear"), this);
    clearAct->setShortcut(QKeySequence(Qt::Key_C));
    clearAct->setStatusTip(tr("Clears the canvas"));
    connect(clearAct, SIGNAL(triggered()), this, SLOT(clear_canvas()));
    m_menu_app->addAction(clearAct);

    QAction* drawLineAct = new QAction(tr("&Line"), this);
    drawLineAct->setShortcut(QKeySequence(Qt::Key_L));
    drawLineAct->setStatusTip(tr("Draws a line"));
    connect(drawLineAct, SIGNAL(triggered()), this, SLOT(set_line()));

    QAction* drawOvalAct = new QAction(tr("&Oval"), this);
    drawOvalAct->setShortcut(QKeySequence(Qt::Key_O));
    drawOvalAct->setStatusTip(tr("Draws an Oval"));
    connect(drawOvalAct, SIGNAL(triggered()), this, SLOT(set_oval()));

    QAction* drawRectangleAct = new QAction(tr("&Rectangle"), this);
    drawRectangleAct->setShortcut(QKeySequence(Qt::Key_R));
    drawRectangleAct->setStatusTip(tr("Draws a rectangle"));
    connect(drawRectangleAct, SIGNAL(triggered()), this, SLOT(set_rect()));

    drawLineAct->setCheckable(true);
    drawOvalAct->setCheckable(true);
    drawRectangleAct->setCheckable(true);  

    QActionGroup* group = new QActionGroup(this);
    drawLineAct->setActionGroup(group);
    drawOvalAct->setActionGroup(group);
    drawRectangleAct->setActionGroup(group);
    drawLineAct->toggle();

    m_menu_tools->addAction(drawLineAct);
    m_menu_tools->addAction(drawOvalAct);
    m_menu_tools->addAction(drawRectangleAct);

    QAction* fillColourBlackAct = new QAction(tr("Black"), this);
    connect(fillColourBlackAct, SIGNAL(triggered()), this, SLOT(set_fill_colour_black()));
    QAction* fillColourRedAct = new QAction(tr("Red"), this);
    connect(fillColourRedAct, SIGNAL(triggered()), this, SLOT(set_fill_colour_red()));
    QAction* fillColourGreenAct = new QAction(tr("Green"), this);
    connect(fillColourGreenAct, SIGNAL(triggered()), this, SLOT(set_fill_colour_green()));
    QAction* fillColourBlueAct = new QAction(tr("Blue"), this);
    connect(fillColourBlueAct, SIGNAL(triggered()), this, SLOT(set_fill_colour_blue()));

    fillColourBlackAct->setCheckable(true);
    fillColourRedAct->setCheckable(true);
    fillColourGreenAct->setCheckable(true);
    fillColourBlueAct->setCheckable(true);

    QActionGroup* colourGroup = new QActionGroup(this);
    fillColourBlackAct->setActionGroup(colourGroup);
    fillColourRedAct->setActionGroup(colourGroup);
    fillColourGreenAct->setActionGroup(colourGroup);
    fillColourBlueAct->setActionGroup(colourGroup);
    fillColourBlackAct->toggle();

    m_menu_colour->addAction(fillColourBlackAct);
    m_menu_colour->addAction(fillColourRedAct);
    m_menu_colour->addAction(fillColourGreenAct);
    m_menu_colour->addAction(fillColourBlueAct);

    QAction* helpLineAct = new QAction(tr("&Line Help"), this);
    helpLineAct->setStatusTip(tr("Help Instructions"));
    connect(helpLineAct, SIGNAL(triggered()), this, SLOT(help_line()));
    m_menu_help->addAction(helpLineAct);

    QAction* helpOvalAct = new QAction(tr("Oval Help"), this);
    connect(helpOvalAct, SIGNAL(triggered()), this, SLOT(help_oval()));
    m_menu_help->addAction(helpOvalAct);

    QAction* helpRectAct = new QAction(tr("Rectangle Help"), this);
    connect(helpRectAct, SIGNAL(triggered()), this, SLOT(help_rect()));
    m_menu_help->addAction(helpRectAct);
}

void PaintWindow::createButton() {
    m_button_quit = new QPushButton("Quit", this);
    //m_button_quit->setGeometry(QRect(QPoint(0, 0), QSize(200, 50)));
    connect(m_button_quit, SIGNAL(released()), this, SLOT(close()));
}

void displayMessage(const char* message) {
        QMessageBox msgBox;
    msgBox.setText(QString(message));
    msgBox.exec();
}

void PaintWindow::help_line() {
    const char* message =
    "Drawing a Line\n"
    "\n"
    "To draw a line, press the left mouse button to mark the beginning of the line.  Drag the mouse to the end of the line and release the button.";

    displayMessage(message);
}

void PaintWindow::help_rect() {
    const char* message =
    "Drawing a Rectangle\n"
    "\n"
    "To draw a rectangle, press the left mouse button to mark the first corner of the rectangle. Drag the mouse to the opposite corner and release the button.";

    displayMessage(message);
}

void PaintWindow::help_oval() {
    const char* message =
    "Drawing an Oval\n"
    "\n"
    "To draw an oval, press the left mouse button to mark the first corner of the oval. Drag the mouse to the opposite corner and release the button.";

    displayMessage(message);
}

void PaintWindow::set_line() {
    m_canvas->set_mode(PaintCanvas::DRAW_LINE);
}

void PaintWindow::set_oval() {
    m_canvas->set_mode(PaintCanvas::DRAW_OVAL);
}

void PaintWindow::set_rect() {
    m_canvas->set_mode(PaintCanvas::DRAW_RECTANGLE);
}

void PaintWindow::set_fill_colour_black() {
    m_canvas->set_fill_colour_fromRgb(0, 0, 0);
}

void PaintWindow::set_fill_colour_red() {
    m_canvas->set_fill_colour_fromRgb(255, 0, 0);
}

void PaintWindow::set_fill_colour_green() {
    m_canvas->set_fill_colour_fromRgb(0, 255, 0);
}

void PaintWindow::set_fill_colour_blue() {
    m_canvas->set_fill_colour_fromRgb(0, 0, 255);
}

void PaintWindow::clear_canvas() {
    m_canvas->clear_canvas();
}
