#include <QtWidgets>
#include <QGLFormat>
#include <iostream>
#include "AppWindow.hpp"

AppWindow::AppWindow() {
    setWindowTitle("488 Tetrominoes on the Wall");

    QGLFormat glFormat;
    glFormat.setVersion(3,3);
    glFormat.setProfile(QGLFormat::CoreProfile);
    glFormat.setSampleBuffers(true);

    QVBoxLayout *layout = new QVBoxLayout;
    // m_menubar = new QMenuBar;
    m_viewer = new Viewer(glFormat, this);
    layout->addWidget(m_viewer);
    setCentralWidget(new QWidget);
    centralWidget()->setLayout(layout);
    m_viewer->show();

    createActions();
    createMenu();
}

void AppWindow::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Escape) {
        QCoreApplication::instance()->quit();
    } else if (event->key() == Qt::Key_Up) {
        m_viewer->rotatePieceCCW();
    } else if (event->key() == Qt::Key_Down) {
        m_viewer->rotatePieceCW();
    } else if (event->key() == Qt::Key_Left) {
        m_viewer->movePieceLeft();
    } else if (event->key() == Qt::Key_Right) {
        m_viewer->movePieceRight();
    } else if (event->key() == Qt::Key_Space) {
        m_viewer->dropPiece();
    } else {
        QWidget::keyPressEvent(event);
    }
}

void AppWindow::createActions() {
    // Application Menu
    // New Game option
    QAction* newGameAct = new QAction(tr("&New Game"), this);
    m_menu_app_actions.push_back(newGameAct);
    newGameAct->setShortcut(QKeySequence(Qt::Key_N));
    newGameAct->setStatusTip(tr("Starts a new game"));
    connect(newGameAct, SIGNAL(triggered()), this, SLOT(new_game()));

    // Reset option
    QAction* resetAct = new QAction(tr("&Reset"), this);
    m_menu_app_actions.push_back(resetAct);
    resetAct->setShortcut(QKeySequence(Qt::Key_R));
    resetAct->setStatusTip(tr("Resets the view of the game"));
    connect(resetAct, SIGNAL(triggered()), this, SLOT(reset_view()));

    // Creates a new action for quiting and pushes it onto the menu actions vector 
    QAction* quitAct = new QAction(tr("&Quit"), this);
    m_menu_app_actions.push_back(quitAct);
    // We set the accelerator keys
    // Alternatively, you could use: setShortcuts(Qt::CTRL + Qt::Key_P); 
    quitAct->setShortcuts(QKeySequence::Quit);
    // Set the tip
    quitAct->setStatusTip(tr("Exits the file"));
    // Connect the action with the signal and slot designated
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));

    // Draw Mode menu
    // Wire-frame
    QAction* wireframeAct = new QAction(tr("&Wire-frame"), this);
    m_menu_draw_actions.push_back(wireframeAct);
    wireframeAct->setShortcut(QKeySequence(Qt::Key_W));
    wireframeAct->setCheckable(true);
    connect(wireframeAct, SIGNAL(triggered()), this, SLOT(set_draw_wire()));

    // Face
    QAction* faceAct = new QAction(tr("&Face"), this);
    m_menu_draw_actions.push_back(faceAct);
    faceAct->setShortcut(QKeySequence(Qt::Key_F));
    faceAct->setCheckable(true);
    connect(faceAct, SIGNAL(triggered()), this, SLOT(set_draw_face()));

    // Multicoloured
    QAction* multicolouredAct = new QAction(tr("&Multicoloured"), this);
    m_menu_draw_actions.push_back(multicolouredAct);
    multicolouredAct->setShortcut(QKeySequence(Qt::Key_M));
    multicolouredAct->setCheckable(true);
    connect(multicolouredAct, SIGNAL(triggered()), this, SLOT(set_draw_multi()));

    QActionGroup* drawGroup = new QActionGroup(this);
    wireframeAct->setActionGroup(drawGroup);
    faceAct->setActionGroup(drawGroup);
    multicolouredAct->setActionGroup(drawGroup);
    faceAct->toggle();

    // Speed Mode menu
    // Slow
    QAction* slowAct = new QAction(tr("Slow"), this);
    m_menu_speed_actions.push_back(slowAct);
    slowAct->setShortcut(QKeySequence(Qt::Key_1));
    slowAct->setCheckable(true);
    connect(slowAct, SIGNAL(triggered()), this, SLOT(set_speed_slow()));

    // Medium
    QAction* mediumAct = new QAction(tr("Medium"), this);
    m_menu_speed_actions.push_back(mediumAct);
    mediumAct->setShortcut(QKeySequence(Qt::Key_2));
    mediumAct->setCheckable(true);
    connect(mediumAct, SIGNAL(triggered()), this, SLOT(set_speed_medium()));

    // Fast
    QAction* fastAct = new QAction(tr("Fast"), this);
    m_menu_speed_actions.push_back(fastAct);
    fastAct->setShortcut(QKeySequence(Qt::Key_3));
    fastAct->setCheckable(true);
    connect(fastAct, SIGNAL(triggered()), this, SLOT(set_speed_fast()));

    QActionGroup* speedGroup = new QActionGroup(this);
    slowAct->setActionGroup(speedGroup);
    mediumAct->setActionGroup(speedGroup);
    fastAct->setActionGroup(speedGroup);
    mediumAct->toggle();

}

void AppWindow::createMenu() {
    m_menu_app = menuBar()->addMenu(tr("&Application"));
    m_menu_draw = menuBar()->addMenu(tr("Draw Mode"));
    m_menu_speed = menuBar()->addMenu(tr("Speed"));

    for (auto& action : m_menu_app_actions) {
        m_menu_app->addAction(action);
    }

    for (auto& action : m_menu_draw_actions) {
        m_menu_draw->addAction(action);
    }

    for (auto& action : m_menu_speed_actions) {
        m_menu_speed->addAction(action);
    }
}

void AppWindow::new_game() {
    m_viewer->newGame();
}

void AppWindow::reset_view() {
    m_viewer->resetView();
}

void AppWindow::set_draw_wire() {
   m_viewer->setDrawMode(Viewer::WIREFRAME); 
}

void AppWindow::set_draw_face() {
    m_viewer->setDrawMode(Viewer::FACE);
}

void AppWindow::set_draw_multi() {
    m_viewer->setDrawMode(Viewer::MULTICOLOURED);
}

void AppWindow::set_speed_slow() {
    m_viewer->setSpeed(1000);
}

void AppWindow::set_speed_medium() {
    m_viewer->setSpeed(500);
}

void AppWindow::set_speed_fast() {
    m_viewer->setSpeed(250);
}

