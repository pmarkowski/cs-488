#ifndef APPWINDOW_HPP
#define APPWINDOW_HPP

#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <vector>
#include "Viewer.hpp"

class AppWindow : public QMainWindow
{
    Q_OBJECT

public:
    AppWindow();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void new_game();
    void reset_view();

    void set_draw_wire();
    void set_draw_face();
    void set_draw_multi();

    void set_speed_slow();
    void set_speed_medium();
    void set_speed_fast();

private:
    void createActions();
    void createMenu();

    // Each menu itself
    QMenu* m_menu_app;
    QMenu* m_menu_draw;
    QMenu* m_menu_speed;

    std::vector<QAction*> m_menu_app_actions;
    std::vector<QAction*> m_menu_draw_actions;
    std::vector<QAction*> m_menu_speed_actions;
    Viewer* m_viewer;
};

#endif
