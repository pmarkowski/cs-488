// #include <GL/glew.h>
#include <QtWidgets>
#include <QtOpenGL>
#include <QVector3D>
#include "Viewer.hpp"
#include <iostream>
#include <math.h>
// #include <GL/gl.h>
#include <GL/glu.h>


#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE 0x809D
#endif

Viewer::Viewer(const QGLFormat& format, QWidget *parent)
    : QGLWidget(format, parent)
#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    , mVertexBufferObject(QOpenGLBuffer::VertexBuffer)
    , mVertexArrayObject(this)
#else
    , mVertexBufferObject(QGLBuffer::VertexBuffer)
#endif
{
    mTimer = new QTimer(this);
    connect(mTimer, SIGNAL(timeout()), this, SLOT(update()));
    mTimer->start(1000/30);

    mGame = new Game(10, 20);
    mGameTimer = new QTimer(this);
    connect(mGameTimer, SIGNAL(timeout()), this, SLOT(gameTick()));
    mGameTimer->start(500);

    mDrawMode = FACE;
    mScale = 1;
}

Viewer::~Viewer() {

}

QSize Viewer::minimumSizeHint() const {
    return QSize(50, 50);
}

QSize Viewer::sizeHint() const {
    return QSize(300, 600);
}

void Viewer::initializeGL() {
    QGLFormat glFormat = QGLWidget::format();
    if (!glFormat.sampleBuffers()) {
        std::cerr << "Could not enable sample buffers." << std::endl;
        return;
    }

    glClearColor(0.7, 0.7, 1.0, 0.0);

    if (!mProgram.addShaderFromSourceFile(QGLShader::Vertex, "shader.vert")) {
        std::cerr << "Cannot load vertex shader." << std::endl;
        return;
    }

    if (!mProgram.addShaderFromSourceFile(QGLShader::Fragment, "shader.frag")) {
        std::cerr << "Cannot load fragment shader." << std::endl;
        return;
    }

    if ( !mProgram.link() ) {
        std::cerr << "Cannot link shaders." << std::endl;
        return;
    }

    float triangleData[] = {
        //  X     Y     Z
         0.0f, 0.0f, 0.0f,  // 'Front'
         1.0f, 0.0f, 0.0f,
         1.0f, 1.0f, 0.0f,
         0.0f, 0.0f, 0.0f,
         0.0f, 1.0f, 0.0f,
         1.0f, 1.0f, 0.0f,

         0.0f, 0.0f, 0.0f,  // 'Top'
         0.0f, 0.0f, -1.0f,
         1.0f, 0.0f, -1.0f,
         0.0f, 0.0f, 0.0f,
         1.0f, 0.0f, 0.0f,
         1.0f, 0.0f, -1.0f,

         0.0f, 0.0f, 0.0f,  // 'Left'
         0.0f, 1.0f, 0.0f,
         0.0f, 1.0f, -1.0f,
         0.0f, 0.0f, 0.0f,
         0.0f, 0.0f, -1.0f,
         0.0f, 1.0f, -1.0f,

         1.0f, 1.0f, -1.0f, // 'Back'
         0.0f, 1.0f, -1.0f,
         0.0f, 0.0f, -1.0f,
         1.0f, 1.0f, -1.0f,
         1.0f, 0.0f, -1.0f,
         0.0f, 0.0f, -1.0f,

         1.0f, 1.0f, -1.0f, // 'Bottom'
         0.0f, 1.0f, -1.0f,
         0.0f, 1.0f, 0.0f,
         1.0f, 1.0f, -1.0f,
         1.0f, 1.0f, 0.0f,
         0.0f, 1.0f, 0.0f,

         1.0f, 1.0f, -1.0f, // 'Right'
         1.0f, 0.0f, -1.0f,
         1.0f, 0.0f, 0.0f,
         1.0f, 1.0f, -1.0f,
         1.0f, 1.0f, 0.0f,
         1.0f, 0.0f, 0.0f
};


#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    mVertexArrayObject.create();
    mVertexArrayObject.bind();

    mVertexBufferObject.create();
    mVertexBufferObject.setUsagePattern(QOpenGLBuffer::StaticDraw);
#else

    /*
     * if qt version is less than 5.1, use the following commented code
     * instead of QOpenGLVertexVufferObject. Also use QGLBuffer instead of
     * QOpenGLBuffer.
     */
    uint vao;

    typedef void (APIENTRY *_glGenVertexArrays) (GLsizei, GLuint*);
    typedef void (APIENTRY *_glBindVertexArray) (GLuint);

    _glGenVertexArrays glGenVertexArrays;
    _glBindVertexArray glBindVertexArray;

    glGenVertexArrays = (_glGenVertexArrays) QGLWidget::context()->getProcAddress("glGenVertexArrays");
    glBindVertexArray = (_glBindVertexArray) QGLWidget::context()->getProcAddress("glBindVertexArray");

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    mVertexBufferObject.create();
    mVertexBufferObject.setUsagePattern(QGLBuffer::StaticDraw);
#endif

    if (!mVertexBufferObject.bind()) {
        std::cerr << "could not bind vertex buffer to the context." << std::endl;
        return;
    }

    mVertexBufferObject.allocate(triangleData, 6 * 6 * 3 * sizeof(float));

    mProgram.bind();

    mProgram.enableAttributeArray("vert");
    mProgram.setAttributeBuffer("vert", GL_FLOAT, 0, 3);

    // mPerspMatrixLocation = mProgram.uniformLocation("cameraMatrix");
    mMvpMatrixLocation = mProgram.uniformLocation("mvpMatrix");
    mFragColour = mProgram.uniformLocation("fragColour");

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void Viewer::setColour(const QVector3D rgb) {
    mProgram.setUniformValue(mFragColour, rgb);
}

void Viewer::setColourForPiece(const int piece) {
    switch(piece) {
        case 0:
            setColour(QVector3D(0.75, 0.0, 0.0));
            break;
        case 1:
            setColour(QVector3D(0.0, 0.75, 0.0));
            break;
        case 2:
            setColour(QVector3D(0.0, 0.0, 0.75));
            break;
        case 3:
            setColour(QVector3D(0.5, 0.5, 0.0));
            break;
        case 4:
            setColour(QVector3D(0.5, 0.0, 0.5));
            break;
        case 5:
            setColour(QVector3D(0.0, 0.5, 0.5));
            break;
        case 6:
            setColour(QVector3D(0.10, 0.10, 0.10));
            break;
        default:
            setColour(QVector3D(0.25, 0.25, 0.25));
            break;
    }
}

void Viewer::drawUnitCube(const QMatrix4x4 modelMatrix) {
     mProgram.setUniformValue(mMvpMatrixLocation, getCameraMatrix() * modelMatrix);
     glDrawArrays(GL_TRIANGLES, 0, 6 * 6);
}

void Viewer::drawMulticolouredCube(const QMatrix4x4 modelMatrix) {
    mProgram.setUniformValue(mMvpMatrixLocation, getCameraMatrix() * modelMatrix);

    // Draw the 6 faces
    for (int i = 0; i < 6; i++) {
        setColourForPiece(i);
        glDrawArrays(GL_TRIANGLES, i * 6, 6);
    }
}

void Viewer::update() {
    QGLWidget::update();
    if (mMouseMode == NONE) {
        rotateWorld(mRotationVector);
    }
}

void Viewer::paintGL() {
    // Clear the screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    mVertexArrayObject.bind();
#endif

    switch (mDrawMode) {
        case WIREFRAME:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            break;
        case FACE:
        case MULTICOLOURED:
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            break;
        default:
            break;
    }

    // Translation matrix for grid
    QMatrix4x4 translationMatrix = QMatrix4x4();
    translationMatrix.translate(-(mGame->getWidth()) / 2.0, mGame->getHeight() / 2);

    // Draw well
    // Set well colour
    setColour(QVector3D(0.5f, 0.5f, 0.5f));

    // Sides
    QMatrix4x4 wellVertMatrix = QMatrix4x4();
    wellVertMatrix.translate(-1, 0);

    for (int i = 0; i < mGame->getHeight(); i++) {

        drawUnitCube(wellVertMatrix * translationMatrix);

        wellVertMatrix.translate(mGame->getWidth() + 1, 0);
        drawUnitCube(wellVertMatrix * translationMatrix);

        wellVertMatrix.translate(-(mGame->getWidth() + 1), -1);
    }

    // Bottom
    for (int i = 0; i < mGame->getWidth() + 2; i++) {
        drawUnitCube(wellVertMatrix * translationMatrix);
        wellVertMatrix.translate(1.0, 0);
    }

    // Draw game grid
    QMatrix4x4 pieceMatrix = QMatrix4x4();
    pieceMatrix.translate(0, 4);
    for (int i = mGame->getHeight() + 3; i >= 0; i--) {
        for (int j = 0; j < mGame->getWidth(); j++) {
            int gridPiece = mGame->get(i, j);

            if (gridPiece != -1) {
                if (mDrawMode == MULTICOLOURED) {
                    drawMulticolouredCube(pieceMatrix * translationMatrix);
                } else {
                    setColourForPiece(gridPiece);
                    drawUnitCube(pieceMatrix * translationMatrix);
                }
            }

            pieceMatrix.translate(1, 0);
        }
        pieceMatrix.translate(-mGame->getWidth(), -1);
    }
}

void Viewer::resizeGL(int width, int height) {
    if (height == 0) {
        height = 1;
    }

    mPerspMatrix.setToIdentity();
    mPerspMatrix.perspective(60.0, (float) width / (float) height, 0.001, 1000);

    glViewport(0, 0, width, height);
}

void Viewer::mousePressEvent ( QMouseEvent * event ) {
    // Set initial mouse location and rotation vector
    mMouseX = event->x();
    mRotationVector = QVector3D();

    // Set mode based on click
    MouseMode setMode;

    if (event->modifiers() & Qt::ShiftModifier) {
        setMode = SCALE;
    } else {
        switch(event->button()) {
            case (Qt::LeftButton):  // Rotate around X-axis
                setMode = ROTATE_X;
                break;
            case (Qt::RightButton): // Rotate around Y-axis
                setMode = ROTATE_Y;
                break;
            case (Qt::MidButton):   // Rotate around Z-axis
                setMode = ROTATE_Z;
                break;
            default:
                setMode = NONE;
                break;
        }
    }

    mMouseMode = setMode;
}

void Viewer::mouseReleaseEvent ( QMouseEvent * event ) {
    mMouseMode = NONE;
}

void Viewer::mouseMoveEvent ( QMouseEvent * event ) {
    int newMouseX = event->x();
    float mouseDelta = newMouseX - mMouseX;
    float mouseScale = 100;

    switch (mMouseMode) {
        case (ROTATE_X):
            mRotationVector = QVector3D(mouseDelta, 0, 0);
            break;
        case (ROTATE_Y):
            mRotationVector = QVector3D(0, mouseDelta, 0);
            break;
        case (ROTATE_Z):
            mRotationVector = QVector3D(0, 0, mouseDelta);
            break;
        case (SCALE): {
            float scaleBy = 1 + mouseDelta / mouseScale;
            float newScale = mScale * scaleBy;

            if (mMinScale <= newScale && newScale <= mMaxScale) {
                scaleWorld(scaleBy, scaleBy, scaleBy);
                mScale = newScale;
            } else if (mScale < newScale) {
                // snap scale to maxScale
                scaleBy = mMaxScale / mScale;
                mScale = mMaxScale;
                scaleWorld(scaleBy, scaleBy, scaleBy);
            } else if (newScale < mScale) {
                // snap scale to minScale
                scaleBy = mMinScale / mScale;
                mScale = mMinScale;
                scaleWorld(scaleBy, scaleBy, scaleBy);
            }
            break;
        }
        case (NONE):
        default:
            break;
    }

    rotateWorld(mRotationVector);

    mMouseX = newMouseX;
}

QMatrix4x4 Viewer::getCameraMatrix() {
    QMatrix4x4 vMatrix;

    QMatrix4x4 cameraTransformation;
    QVector3D cameraPosition = cameraTransformation * QVector3D(0, 0, 20.0);
    QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 1, 0);

    vMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);

    return mPerspMatrix * vMatrix * mTransformMatrix;
}

void Viewer::translateWorld(float x, float y, float z) {
    mTransformMatrix.translate(x, y, z);
}

void Viewer::rotateWorld(const QVector3D rotationVector) {
    // X axis
    if (rotationVector.x() != 0) {
        rotateWorld(rotationVector.x(), 1, 0, 0);
    }
    // Y axis
    else if (rotationVector.y() != 0) {
        rotateWorld(rotationVector.y(), 0, 1, 0);
    }
    // Z axis
    else if (rotationVector.z() != 0) {
        rotateWorld(rotationVector.z(), 0, 0, 1);
    }
}

void Viewer::rotateWorld(float angle, float x, float y, float z) {
    mTransformMatrix.rotate(angle, x, y, z);
}

void Viewer::scaleWorld(float x, float y, float z) {
    mTransformMatrix.scale(x, y, z);
}

void Viewer::resetView() {
    mTransformMatrix.setToIdentity();
    mRotationVector = QVector3D();
    mScale = 1;
}

void Viewer::setDrawMode(DrawMode mode) {
    mDrawMode = mode;
}

void Viewer::gameTick() {
    mGame->tick();
    update();
}

void Viewer::newGame() {
    mGame->reset();
}

void Viewer::movePieceLeft() {
    mGame->moveLeft();
}

void Viewer::movePieceRight() {
    mGame->moveRight();
}

void Viewer::rotatePieceCW() {
    mGame->rotateCW();
}

void Viewer::rotatePieceCCW() {
    mGame->rotateCCW();
}

void Viewer::dropPiece() {
    mGame->drop();
}

void Viewer::setSpeed(int ms) {
    mGameTimer->start(ms);
}

