#ifndef CS488_VIEWER_HPP
#define CS488_VIEWER_HPP

#include <QGLWidget>
#include <QGLShaderProgram>
#include <QMatrix4x4>
#include <QVector3D>
#include <QtGlobal>

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#else 
#include <QGLBuffer>
#endif

#include "game.hpp"

class Viewer : public QGLWidget {
    
    Q_OBJECT

public:
    Viewer(const QGLFormat& format, QWidget *parent = 0);
    virtual ~Viewer();
    
    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    // If you want to render a new frame, call do not call paintGL(),
    // instead, call update() to ensure that the view gets a paint 
    // event.

    enum DrawMode {
        WIREFRAME,
        FACE,
        MULTICOLOURED
    };

    void setDrawMode(DrawMode mode);
    void resetView();

    void newGame();
    void movePieceLeft();
    void movePieceRight();
    void rotatePieceCW();
    void rotatePieceCCW();
    void dropPiece();
    void setSpeed(int ms);
 
protected slots:
    virtual void update();

protected:

    // Events we implement

    // Called when GL is first initialized
    virtual void initializeGL();
    // Called when our window needs to be redrawn
    virtual void paintGL();
    // Called when the window is resized (formerly on_configure_event)
    virtual void resizeGL(int width, int height);
    // Called when a mouse button is pressed
    virtual void mousePressEvent ( QMouseEvent * event );
    // Called when a mouse button is released
    virtual void mouseReleaseEvent ( QMouseEvent * event );
    // Called when the mouse moves
    virtual void mouseMoveEvent ( QMouseEvent * event );

private slots:
    void gameTick();

private:

    QMatrix4x4 getCameraMatrix();
    void translateWorld(float x, float y, float z);
    void rotateWorld(const QVector3D rotationVector);
    void rotateWorld(float angle, float x, float y, float z);
    void scaleWorld(float x, float y, float z);

    void setColour(const QVector3D rgb);
    void setColourForPiece(const int piece);
    void drawUnitCube(const QMatrix4x4 modelMatrix);
    void drawMulticolouredCube(const QMatrix4x4 modelMatrix);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    QOpenGLBuffer mVertexBufferObject;
    QOpenGLVertexArrayObject mVertexArrayObject;
#else 
    QGLBuffer mVertexBufferObject;
#endif

    int mVertexLocation;
    int mMvpMatrixLocation;
    int mFragColour;

    QMatrix4x4 mPerspMatrix;
    QMatrix4x4 mTransformMatrix;
    
    QTimer* mTimer;
    QGLShaderProgram mProgram;

    Game* mGame;
    QTimer* mGameTimer;

    DrawMode mDrawMode;

    enum MouseMode {
        NONE,
        ROTATE_X,
        ROTATE_Y,
        ROTATE_Z,
        SCALE
    };

    MouseMode mMouseMode;

    int mMouseX;
    QVector3D mRotationVector;

    constexpr static float mMinScale = 0.5f;
    constexpr static float mMaxScale = 2.0f;
    float mScale;
};

#endif
