#version 330

uniform vec3 fragColour;

out vec4 finalColor;

void main()
{
    finalColor = vec4(fragColour, 1.0);
    // gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}
