#include <QtWidgets>
#include <iostream>
#include <string>
#include "AppWindow.hpp"

AppWindow::AppWindow() {
    setWindowTitle("488 Assignment Two");

    QGLFormat glFormat;
    glFormat.setVersion(3,3);
    glFormat.setProfile(QGLFormat::CoreProfile);
    glFormat.setSampleBuffers(true);

    QVBoxLayout *layout = new QVBoxLayout;
    // m_menubar = new QMenuBar;
    m_viewer = new Viewer(glFormat, this);

    connect(m_viewer, SIGNAL(updateNearLabel(double)), this, SLOT(updateNearLabel(double)));
    connect(m_viewer, SIGNAL(updateFarLabel(double)), this, SLOT(updateFarLabel(double)));

    layout->addWidget(m_viewer);
    setCentralWidget(new QWidget);
    centralWidget()->setLayout(layout);

    createActions();
    createMenu();
    createStatusBarLabels();

    resetView();
}

void AppWindow::createActions() {

    // Application Menu
    QAction* resetAct = new QAction(tr("Reset"), this);
    m_menu_actions.push_back(resetAct);
    resetAct->setShortcut(QKeySequence(Qt::Key_A));
    connect(resetAct, SIGNAL(triggered()), this, SLOT(resetView()));

    // Creates a new action for quiting and pushes it onto the menu actions vector 
    QAction* quitAct = new QAction(tr("&Quit"), this);
    m_menu_actions.push_back(quitAct);
    // We set the accelerator keys
    // Alternatively, you could use: setShortcuts(Qt::CTRL + Qt::Key_P); 
    quitAct->setShortcuts(QKeySequence::Quit);
    // Set the tip
    quitAct->setStatusTip(tr("Exits the file"));
    // Connect the action with the signal and slot designated
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));

    // Mode Menu
    // View Mode
    // Rotate
    QAction* viewRotateAct = new QAction(tr("View R&otate"), this);
    m_menu_mode_actions.push_back(viewRotateAct);
    viewRotateAct->setShortcut(QKeySequence(Qt::Key_O));
    connect(viewRotateAct, SIGNAL(triggered()), this, SLOT(setModeViewRotate()));
    // Translate
    QAction* viewTranslateAct = new QAction(tr("View Tra&nslate"), this);
    m_menu_mode_actions.push_back(viewTranslateAct);
    viewTranslateAct->setShortcut(QKeySequence(Qt::Key_N));
    connect(viewTranslateAct, SIGNAL(triggered()), this, SLOT(setModeViewTranslate()));
    // Perspective
    QAction* viewPerspectiveAct = new QAction(tr("View &Perspective"), this);
    m_menu_mode_actions.push_back(viewPerspectiveAct);
    viewPerspectiveAct->setShortcut(QKeySequence(Qt::Key_P));
    connect(viewPerspectiveAct, SIGNAL(triggered()), this, SLOT(setModeViewPerspective()));

    // Separator
    QAction* viewModelSeparatorAct = new QAction(this);
    viewModelSeparatorAct->setSeparator(true);
    m_menu_mode_actions.push_back(viewModelSeparatorAct);

    // Model Mode
    // Rotate (default)
    QAction* modelRotateAct = new QAction(tr("Model &Rotate"), this);
    m_menu_mode_actions.push_back(modelRotateAct);
    modelRotateAct->setShortcut(QKeySequence(Qt::Key_R));
    connect(modelRotateAct, SIGNAL(triggered()), this, SLOT(setModeModelRotate()));
    // Translate
    QAction* modelTranslateAct = new QAction(tr("Model &Translate"), this);
    m_menu_mode_actions.push_back(modelTranslateAct);
    modelTranslateAct->setShortcut(QKeySequence(Qt::Key_T));
    connect(modelTranslateAct, SIGNAL(triggered()), this, SLOT(setModeModelTranslate()));
    // Scale
    QAction* modelScaleAct = new QAction(tr("Model &Scale"), this);
    m_menu_mode_actions.push_back(modelScaleAct);
    modelScaleAct->setShortcut(QKeySequence(Qt::Key_S));
    connect(modelScaleAct, SIGNAL(triggered()), this, SLOT(setModeModelScale()));

    // Separator
    QAction* modelViewportSeparatorAct = new QAction(this);
    modelViewportSeparatorAct->setSeparator(true);
    m_menu_mode_actions.push_back(modelViewportSeparatorAct);

    // Viewport Mode
    QAction* viewportAct = new QAction(tr("&Viewport"), this);
    m_menu_mode_actions.push_back(viewportAct);
    viewportAct->setShortcut(QKeySequence(Qt::Key_V));
    connect(viewportAct, SIGNAL(triggered()), this, SLOT(setModeViewport()));
}

void AppWindow::createMenu() {
    m_menu_app = menuBar()->addMenu(tr("&Application"));
    m_menu_mode = menuBar()->addMenu(tr("Mode"));

    for (auto& action : m_menu_actions) {
        m_menu_app->addAction(action);
    }

    QActionGroup* modeGroup = new QActionGroup(this);
    for (auto& action : m_menu_mode_actions) {
        action->setActionGroup(modeGroup);
        action->setCheckable(true);
        m_menu_mode->addAction(action);
    }
}

void AppWindow::createStatusBarLabels() {
    m_mode_label = new QLabel(tr("Model Rotate"), this);
    m_near_label = new QLabel(tr("Near: 0.1"), this);
    m_far_label = new QLabel(tr("Far: 30"), this);

    statusBar()->addPermanentWidget(m_mode_label);
    statusBar()->addPermanentWidget(m_near_label);
    statusBar()->addPermanentWidget(m_far_label);
}

void AppWindow::updateNearLabel(double near) {
    m_near_label->setText((std::string("Near: ") + std::to_string(near)).c_str());
}

void AppWindow::updateFarLabel(double far) {
    m_far_label->setText((std::string("Far: ") + std::to_string(far)).c_str());
}

// Menu slots
void AppWindow::resetView() {
    m_viewer->reset_view();
    m_menu_mode_actions[4]->trigger();
}

void AppWindow::setModeViewRotate() {
    m_viewer->setMouseMode(Viewer::VIEW_ROTATE);
    m_mode_label->setText("View Rotate");
}

void AppWindow::setModeViewTranslate() {
    m_viewer->setMouseMode(Viewer::VIEW_TRANSLATE);
    m_mode_label->setText("View Translate");
}

void AppWindow::setModeViewPerspective() {
    m_viewer->setMouseMode(Viewer::VIEW_PERSPECTIVE);
    m_mode_label->setText("View Perspective");
}

void AppWindow::setModeModelRotate() {
    m_viewer->setMouseMode(Viewer::MODEL_ROTATE);
    m_mode_label->setText("Model Rotate");
}

void AppWindow::setModeModelTranslate() {
    m_viewer->setMouseMode(Viewer::MODEL_TRANSLATE);
    m_mode_label->setText("Model Translate");
}

void AppWindow::setModeModelScale() {
    m_viewer->setMouseMode(Viewer::MODEL_SCALE);
    m_mode_label->setText("Model Scale");
}

void AppWindow::setModeViewport() {
    m_viewer->setMouseMode(Viewer::VIEWPORT);
    m_mode_label->setText("Viewport");
}

