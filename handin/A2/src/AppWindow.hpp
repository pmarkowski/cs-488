#ifndef APPWINDOW_HPP
#define APPWINDOW_HPP

#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QLabel>
#include <vector>
#include "Viewer.hpp"

class AppWindow : public QMainWindow
{
    Q_OBJECT

public:
    AppWindow();

public slots:
    void updateNearLabel(double near);
    void updateFarLabel(double far);

private slots:
    void resetView();

    void setModeViewRotate();
    void setModeViewTranslate();
    void setModeViewPerspective();

    void setModeModelRotate();
    void setModeModelTranslate();
    void setModeModelScale();

    void setModeViewport();

private:
    void createActions();
    void createMenu();
    void createStatusBarLabels();

    // Each menu itself
    QMenu* m_menu_app;
    QMenu* m_menu_mode;

    std::vector<QAction*> m_menu_actions;
    std::vector<QAction*> m_menu_mode_actions;

    // Status bar labels
    QLabel* m_mode_label;
    QLabel* m_near_label;
    QLabel* m_far_label;

    Viewer* m_viewer;
};

#endif
