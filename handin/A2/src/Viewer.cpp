#include <QtWidgets>
#include <QtOpenGL>
#include <iostream>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include "Viewer.hpp"
// #include "draw.hpp"

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE 0x809D
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// Outcodes
const int TOP    = 1;
const int BOTTOM = 2;
const int RIGHT  = 4;
const int LEFT   = 8;
const int FAR    = 16;
const int NEAR   = 32;

// Default values
const double DEFAULT_FOV        = 30.0;
const double DEFAULT_ASPECT     = 1.0;
const double DEFAULT_NEAR_PLANE = 0.01;
const double DEFAULT_FAR_PLANE  = 10.0;

const QVector2D DEFAULT_VIEWPORT_TOP_LEFT     = QVector2D(-0.95, 0.95);
const QVector2D DEFAULT_VIEWPORT_BOTTOM_RIGHT = QVector2D(0.95, -0.95);

using namespace std;

float degToRad(float deg) {
    return (deg * M_PI / 180);
}

float cot(float rad) {
    return cos(rad) / sin(rad);
}

QMatrix4x4 translationMatrix(float x, float y, float z) {
    return QMatrix4x4(1, 0, 0, x,
                      0, 1, 0, y,
                      0, 0, 1, z,
                      0, 0, 0, 1);
}

QMatrix4x4 scaleMatrix(float x, float y, float z) {
    return QMatrix4x4(x, 0, 0, 0,
                      0, y, 0, 0,
                      0, 0, z, 0,
                      0, 0, 0, 1);
}

QMatrix4x4 rotationMatrix(float angle, bool x, bool y, bool z) {
    float theta = degToRad(angle);
    QMatrix4x4 rotationMatrix = QMatrix4x4();

    if (x) {
        rotationMatrix *= QMatrix4x4(1, 0, 0, 0,
                                     0, cos(theta), -sin(theta), 0,
                                     0, sin(theta), cos(theta), 0,
                                     0, 0, 0, 1);
    }
    if (y) {
        rotationMatrix *= QMatrix4x4(cos(theta), 0, sin(theta), 0,
                                     0, 1, 0, 0,
                                     -sin(theta), 0, cos(theta), 0,
                                     0, 0, 0, 1);
    }
    if (z) {
        rotationMatrix *= QMatrix4x4(cos(theta), -sin(theta), 0, 0,
                                     sin(theta), cos(theta), 0, 0,
                                     0, 0, 1, 0,
                                     0, 0, 0, 1);
    }
    
    return rotationMatrix;
}

Viewer::Viewer(const QGLFormat& format, QWidget *parent) 
    : QGLWidget(format, parent) 
#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    , mVertexBufferObject(QOpenGLBuffer::VertexBuffer)
    , mVertexArrayObject(this)
#else 
    , mVertexBufferObject(QGLBuffer::VertexBuffer)
#endif
{
    // Set default Mouse Mode
    mMouseMode = MODEL_ROTATE;
    // Set default View Matrix
    setViewMatrix();
    // Set perspective matrix
    set_perspective(DEFAULT_FOV, DEFAULT_ASPECT, DEFAULT_NEAR_PLANE, DEFAULT_FAR_PLANE);
    // Create default viewport
    mViewportTopLeft = new QVector2D();
    mViewportBottomRight = new QVector2D();

    // Create Gnomon
    mGnomon = new QVector4D[6];
    QVector4D initGnomon [] = {
        QVector4D(0.0, 0.0, 0.0, 1.0), QVector4D(0.5, 0.0, 0.0, 1.0), // X
        QVector4D(0.0, 0.0, 0.0, 1.0), QVector4D(0.0, 0.5, 0.0, 1.0), // Y
        QVector4D(0.0, 0.0, 0.0, 1.0), QVector4D(0.0, 0.0, 0.5, 1.0)  // Z
    };
    memcpy(mGnomon, initGnomon, sizeof(QVector4D) * 6);

    // Create Cube
    mCube = new QVector4D[24];
    QVector4D initCube [] = {
        QVector4D( 1.0,  1.0,  1.0, 1.0), QVector4D(-1.0,  1.0,  1.0, 1.0),
        QVector4D( 1.0,  1.0,  1.0, 1.0), QVector4D( 1.0, -1.0,  1.0, 1.0),
        QVector4D( 1.0,  1.0,  1.0, 1.0), QVector4D( 1.0,  1.0, -1.0, 1.0),
        QVector4D(-1.0, -1.0, -1.0, 1.0), QVector4D( 1.0, -1.0, -1.0, 1.0),
        QVector4D(-1.0, -1.0, -1.0, 1.0), QVector4D(-1.0,  1.0, -1.0, 1.0),
        QVector4D(-1.0, -1.0, -1.0, 1.0), QVector4D(-1.0, -1.0,  1.0, 1.0),
        QVector4D(-1.0,  1.0, -1.0, 1.0), QVector4D( 1.0,  1.0, -1.0, 1.0),
        QVector4D(-1.0,  1.0, -1.0, 1.0), QVector4D(-1.0,  1.0,  1.0, 1.0),
        QVector4D(-1.0, -1.0,  1.0, 1.0), QVector4D(-1.0,  1.0,  1.0, 1.0),
        QVector4D(-1.0, -1.0,  1.0, 1.0), QVector4D( 1.0, -1.0,  1.0, 1.0),
        QVector4D( 1.0, -1.0, -1.0, 1.0), QVector4D( 1.0,  1.0, -1.0, 1.0),
        QVector4D( 1.0, -1.0, -1.0, 1.0), QVector4D( 1.0, -1.0,  1.0, 1.0)
    };
    memcpy(mCube, initCube, sizeof(QVector4D) * 24);
}

Viewer::~Viewer() {
    delete [] mGnomon;
    delete [] mCube;
}

QSize Viewer::minimumSizeHint() const {
    return QSize(50, 50);
}

QSize Viewer::sizeHint() const {
    return QSize(300, 300);
}

void Viewer::setViewMatrix() {
    QVector3D lookFrom = QVector3D(3, 4, -2);
    QVector3D lookAt = QVector3D(0, 0, 0);
    QVector3D up = QVector3D(0, 1, 0);

    QVector3D v_z = (lookAt - lookFrom).normalized();
    QVector3D v_x = QVector3D::crossProduct(up, v_z).normalized();
    QVector3D v_y = QVector3D::crossProduct(v_z, v_x);

    QMatrix4x4 orientationMatrix = QMatrix4x4(v_x.x(), v_x.y(), v_x.z(), 0,
                                              v_y.x(), v_y.y(), v_y.z(), 0,
                                              v_z.x(), v_z.y(), v_z.z(), 0,
                                              0, 0, 0, 1);

    mViewMatrix = orientationMatrix * translationMatrix(-lookFrom.x(), -lookFrom.y(), -lookFrom.z());;
}

void Viewer::set_perspective(double fov, double aspect,
                             double near, double far)
{
    mFov = fov;
    mAspect = aspect;
    mNear = near;
    mFar = far;

    emit updateNearLabel(mNear);
    emit updateFarLabel(mFar);
}

void Viewer::reset_view()
{
    mMouseMode = MODEL_ROTATE;
    mModelMatrix.setToIdentity();
    setViewMatrix();
    set_perspective(DEFAULT_FOV, DEFAULT_ASPECT, DEFAULT_NEAR_PLANE, DEFAULT_FAR_PLANE);
    *mViewportTopLeft = DEFAULT_VIEWPORT_TOP_LEFT;
    *mViewportBottomRight = DEFAULT_VIEWPORT_BOTTOM_RIGHT;

    update();
}

void Viewer::initializeGL() {
    // Do some OpenGL setup
    QGLFormat glFormat = QGLWidget::format();
    if (!glFormat.sampleBuffers()) {
        std::cerr << "Could not enable sample buffers." << std::endl;
        return;
    }

    glClearColor(0.7, 0.7, 0.7, 0.0);
    
    if (!mProgram.addShaderFromSourceFile(QGLShader::Vertex, "shader.vert")) {
        std::cerr << "Cannot load vertex shader." << std::endl;
        return;
    }

    if (!mProgram.addShaderFromSourceFile(QGLShader::Fragment, "shader.frag")) {
        std::cerr << "Cannot load fragment shader." << std::endl;
        return;
    }

    if ( !mProgram.link() ) {
        std::cerr << "Cannot link shaders." << std::endl;
        return;
    }

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    mVertexArrayObject.create();
    mVertexArrayObject.bind();

    mVertexBufferObject.create();
    mVertexBufferObject.setUsagePattern(QOpenGLBuffer::StaticDraw);
#else 

    /*
     * if qt version is less than 5.1, use the following commented code
     * instead of QOpenGLVertexVufferObject. Also use QGLBuffer instead of 
     * QOpenGLBuffer.
     */
    uint vao;
     
    typedef void (APIENTRY *_glGenVertexArrays) (GLsizei, GLuint*);
    typedef void (APIENTRY *_glBindVertexArray) (GLuint);
     
    _glGenVertexArrays glGenVertexArrays;
    _glBindVertexArray glBindVertexArray;
     
    glGenVertexArrays = (_glGenVertexArrays) QGLWidget::context()->getProcAddress("glGenVertexArrays");
    glBindVertexArray = (_glBindVertexArray) QGLWidget::context()->getProcAddress("glBindVertexArray");
     
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);    

    mVertexBufferObject.create();
    mVertexBufferObject.setUsagePattern(QGLBuffer::DynamicDraw);
#endif

    if (!mVertexBufferObject.bind()) {
        std::cerr << "could not bind vertex buffer to the context." << std::endl;
        return;
    }

    mProgram.bind();

    mProgram.enableAttributeArray("vert");
    mProgram.setAttributeBuffer("vert", GL_FLOAT, 0, 3);

    mColorLocation = mProgram.uniformLocation("frag_color");
}

void clipPoints(QVector4D* point1, QVector4D* point2) {
    // Pre-computations
    // Point 1
    int point1Code = 0;
    float BL_1 = point1->w() + point1->x(); // Left check
    float BR_1 = point1->w() - point1->x(); // Right check
    float BB_1 = point1->w() + point1->y(); // Bottom check
    float BT_1 = point1->w() - point1->y(); // Top check
    float BN_1 = point1->w() + point1->z(); // Near check
    float BF_1 = point1->w() - point1->z(); // Far check

    if (BL_1 < 0) {
        point1Code |= LEFT;
    }
    if (BR_1 < 0) {
        point1Code |= RIGHT;
    }
    if (BB_1 < 0) {
        point1Code |= BOTTOM;
    }
    if (BT_1 < 0) {
        point1Code |= TOP;
    }
    if (BN_1 < 0) {
        point1Code |= NEAR;
    }
    if (BF_1 < 0) {
        point1Code |= FAR;
    }

    // Point 2
    int point2Code = 0;
    float BL_2 = point2->w() + point2->x(); // Left check
    float BR_2 = point2->w() - point2->x(); // Right check
    float BB_2 = point2->w() + point2->y(); // Bottom check
    float BT_2 = point2->w() - point2->y(); // Top check
    float BN_2 = point2->w() + point2->z(); // Near check
    float BF_2 = point2->w() - point2->z(); // Far check

    if (BL_2 < 0) {
        point2Code |= LEFT;
    }
    if (BR_2 < 0) {
        point2Code |= RIGHT;
    }
    if (BB_2 < 0) {
        point2Code |= BOTTOM;
    }
    if (BT_2 < 0) {
        point2Code |= TOP;
    }
    if (BN_2 < 0) {
        point2Code |= NEAR;
    }
    if (BF_2 < 0) {
        point2Code |= FAR;
    }

    if (!(point1Code | point2Code)) {
        // trivially accept
        return;
    } else if ((point1Code & point2Code)) {
        // trivially reject
        point1->setX(0);
        point1->setY(0);
        point1->setZ(0);
        point1->setW(0);
        point2->setX(0);
        point2->setY(0);
        point2->setZ(0);
        point2->setW(0);
    } else {
        // X
        if (point1Code & LEFT) {
            float a = BL_1 / (BL_1 - BL_2);
            *point1 = (1-a) * *point1 + a * *point2;
        }
        if (point1Code & RIGHT) {
            float a = BR_1 / (BR_1 - BR_2);
            *point1 = (1-a) * *point1 + a * *point2;
        }
        if (point2Code & LEFT) {
            float a = BL_2 / (BL_2 - BL_1);
            *point2 = (1-a) * *point2 + a * *point1;
        }
        if (point2Code & RIGHT) {
            float a = BR_2 / (BR_2 - BR_1);
            *point2 = (1-a) * *point2 + a * *point1;
        }

        // Y
        if (point1Code & BOTTOM) {
            float a = BB_1 / (BB_1 - BB_2);
            *point1 = (1-a) * *point1 + a * *point2;
        }
        if (point1Code & TOP) {
            float a = BT_1 / (BT_1 - BT_2);
            *point1 = (1-a) * *point1 + a * *point2;
        }
        if (point2Code & BOTTOM) {
            float a = BB_2 / (BB_2 - BB_1);
            *point2 = (1-a) * *point2 + a * *point1;
        }
        if (point2Code & TOP) {
            float a = BT_2 / (BT_2 - BT_1);
            *point2 = (1-a) * *point2 + a * *point1;
        }

        // Z
        if (point1Code & NEAR) {
            float a = BN_1 / (BN_1 - BN_2);
            *point1 = (1-a) * *point1 + a * *point2;
        }
        if (point1Code & FAR) {
            float a = BF_1 / (BF_1 - BF_2);
            *point1 = (1-a) * *point1 + a * *point2;
        }
        if (point2Code & NEAR) {
            float a = BN_2 / (BN_2 - BN_1);
            *point2 = (1-a) * *point2 + a * *point1;
        }
        if (point2Code & FAR) {
            float a = BF_2 / (BF_2 - BF_1);
            *point2 = (1-a) * *point2 + a * *point1;
        }
    }
}

void Viewer::transformVertices(QVector4D* vertexArray, int arrayLength, QMatrix4x4 matrix) {
    // Apply matrix transformation and projection

    QMatrix4x4 projectionMatrix = QMatrix4x4(cot(degToRad(mFov / 2))/mAspect, 0, 0, 0,
                                             0, cot(degToRad(mFov / 2)), 0, 0,
                                             0, 0, (mFar + mNear) / (mFar - mNear), (-2 * mFar * mNear) / (mFar - mNear),
                                             0, 0, 1, 0);

    for (int i = 0; i < arrayLength; i++) {
        vertexArray[i] = projectionMatrix * matrix * vertexArray[i];
    }

    // Clip
    for (int i = 0; i < arrayLength; i += 2) {
        clipPoints(&vertexArray[i], &vertexArray[i+1]);
    }

    // Homogenize
    for (int i = 0; i < arrayLength; i++) {
        vertexArray[i] /= vertexArray[i].w();
    }
}

QVector2D Viewer::getViewportCoordinates(QVector4D point) {
    float viewportWidth = mViewportBottomRight->x() - mViewportTopLeft->x();
    float viewportHeight = mViewportTopLeft->y() - mViewportBottomRight->y();

    return QVector2D((viewportWidth / 2.0) * (point.x() - (-1)) + mViewportTopLeft->x(),
                     (viewportHeight / 2.0) * (point.y() - (1)) + mViewportTopLeft->y());
}

void Viewer::paintGL() {    
    draw_init();

    // For now we'll just ignore Z coordinate
    // Draw Model Gnomon
    // TODO: make scaling not effect the gnomon
    QVector4D* transformedGnomon = new QVector4D [6];
    memcpy(transformedGnomon, mGnomon, sizeof(QVector4D) * 6);
    transformVertices(transformedGnomon, 6, mViewMatrix * mModelMatrix);

    set_colour(QColor(1.0, 0.0, 0.0));
    if (!transformedGnomon[0].isNull() && !transformedGnomon[1].isNull()) {
        draw_line(getViewportCoordinates(transformedGnomon[0]), getViewportCoordinates(transformedGnomon[1]));
    }
    set_colour(QColor(0.0, 1.0, 0.0));
    if (!transformedGnomon[2].isNull() && !transformedGnomon[3].isNull()) {
        draw_line(getViewportCoordinates(transformedGnomon[2]), getViewportCoordinates(transformedGnomon[3]));
    }
    set_colour(QColor(0.0, 0.0, 1.0));
    if (!transformedGnomon[4].isNull() && !transformedGnomon[5].isNull()) {
        draw_line(getViewportCoordinates(transformedGnomon[4]), getViewportCoordinates(transformedGnomon[5]));
    }

    // Draw Model Cube
    QVector4D* transformedCube = new QVector4D [24];
    memcpy(transformedCube, mCube, sizeof(QVector4D) * 24);
    transformVertices(transformedCube, 24, mViewMatrix * mModelMatrix);

    set_colour(QColor(1.0, 0.0, 1.0));
    for (int i = 0; i < 24; i += 2) {
        if (!transformedCube[i].isNull() && !transformedCube[i+1].isNull()) {
            draw_line(getViewportCoordinates(transformedCube[i]), getViewportCoordinates(transformedCube[i+1]));
        }
    }

    // Draw World Gnomon
    memcpy(transformedGnomon, mGnomon, sizeof(QVector4D) * 6);
    transformVertices(transformedGnomon, 6, mViewMatrix);

    set_colour(QColor(1.0, 0.0, 0.0));
    if (!transformedGnomon[0].isNull() && !transformedGnomon[1].isNull()) {
        draw_line(getViewportCoordinates(transformedGnomon[0]), getViewportCoordinates(transformedGnomon[1]));
    }
    set_colour(QColor(0.0, 1.0, 0.0));
    if (!transformedGnomon[0].isNull() && !transformedGnomon[1].isNull()) {
        draw_line(getViewportCoordinates(transformedGnomon[2]), getViewportCoordinates(transformedGnomon[3]));
    }
    set_colour(QColor(0.0, 0.0, 1.0));
    if (!transformedGnomon[0].isNull() && !transformedGnomon[1].isNull()) {
        draw_line(getViewportCoordinates(transformedGnomon[4]), getViewportCoordinates(transformedGnomon[5]));
    }

    // Draw viewport
    set_colour(QColor(0.0, 1.0, 1.0));
    // create top right and bottom left vertices
    QVector2D viewportBottomLeft = QVector2D(mViewportTopLeft->x(), mViewportBottomRight->y());
    QVector2D viewportTopRight = QVector2D(mViewportBottomRight->x(), mViewportTopLeft->y());
    // Draw lines
    draw_line(*mViewportTopLeft, viewportTopRight);
    draw_line(viewportTopRight, *mViewportBottomRight);
    draw_line(*mViewportBottomRight, viewportBottomLeft);
    draw_line(viewportBottomLeft, *mViewportTopLeft);
}

QVector2D Viewer::getWindowMouseCoordinate(float x, float y) {
    return QVector2D(-1.0 + 2.0 / width() * x, 1.0 + -2.0 / height() * y);
}

void Viewer::normalizeViewportCorners() {
    float maxX = fmax(mViewportTopLeft->x(), mViewportBottomRight->x());
    float minX = fmin(mViewportTopLeft->x(), mViewportBottomRight->x());

    float maxY = fmax(mViewportTopLeft->y(), mViewportBottomRight->y());
    float minY = fmin(mViewportTopLeft->y(), mViewportBottomRight->y());

    *mViewportTopLeft = QVector2D(minX, maxY);
    *mViewportBottomRight = QVector2D(maxX, minY);
}

void Viewer::setMouseMode(MouseMode mode) {
    mMouseMode = mode;
}

void Viewer::mousePressEvent ( QMouseEvent * event ) {
    mPreviousMouseX = event->x();
    if (mMouseMode == VIEWPORT && event->buttons() & Qt::LeftButton) {
        *mViewportTopLeft = getWindowMouseCoordinate(event->x(), event->y());
    }
}

void Viewer::mouseReleaseEvent ( QMouseEvent * event ) {
    if (mMouseMode == VIEWPORT && event->button() == Qt::LeftButton) {
        float xPos = event->x();
        float yPos = event->y();

        if (xPos < 0) {
            xPos = 0;
        } else if (xPos > width()) {
            xPos = width();
        }

        if (yPos < 0) {
            yPos = 0;
        } else if (yPos > height()) {
            yPos = height();
        }

        *mViewportBottomRight = getWindowMouseCoordinate(xPos, yPos);
        normalizeViewportCorners();

        update();
    }
}

void Viewer::mouseMoveEvent ( QMouseEvent * event ) {
    int newMouseX = event->x();
    float mouseDelta = (newMouseX - mPreviousMouseX) / 1000.0 ;
    switch (mMouseMode) {
        case VIEW_ROTATE:
            mouseDelta *= 100;
            mViewMatrix = rotationMatrix(
                            mouseDelta,
                            (event->buttons() & Qt::LeftButton),
                            (event->buttons() & Qt::MidButton),
                            (event->buttons() & Qt::RightButton)).inverted()
                          * mViewMatrix;

            break;
        case VIEW_TRANSLATE: {
            mViewMatrix = translationMatrix(
                            mouseDelta * (bool)(event->buttons() & Qt::LeftButton),
                            mouseDelta * (bool)(event->buttons() & Qt::MidButton),
                            mouseDelta * (bool)(event->buttons() & Qt::RightButton)).inverted()
                          * mViewMatrix;
            break;
        }
        case VIEW_PERSPECTIVE:
            if (event->buttons() & Qt::LeftButton) {
                double fovChange = mouseDelta * 100;
                if (mFov + fovChange < 5) {
                    mFov = 5.0;
                } else if (mFov + fovChange > 160) {
                    mFov = 160;
                } else {
                    mFov += fovChange;
                }
            }
            if (event->buttons() & Qt::MidButton) {
                mNear += mouseDelta;
                emit updateNearLabel(mNear);
            }
            if (event->buttons() & Qt::RightButton) {
                mFar += mouseDelta;
                emit updateFarLabel(mFar);
            }
            break;
        case MODEL_ROTATE: {
            mouseDelta *= 100;
            mModelMatrix *= rotationMatrix(
                                mouseDelta,
                                (event->buttons() & Qt::LeftButton),
                                (event->buttons() & Qt::MidButton),
                                (event->buttons() & Qt::RightButton));
            break;
        }
        case MODEL_TRANSLATE: {
            mModelMatrix *= translationMatrix(
                                mouseDelta * (bool)(event->buttons() & Qt::LeftButton),
                                mouseDelta * (bool)(event->buttons() & Qt::MidButton),
                                mouseDelta * (bool)(event->buttons() & Qt::RightButton));
            break;
        }
        case MODEL_SCALE: {
            mModelMatrix *= scaleMatrix(
                                1 + mouseDelta * (bool)(event->buttons() & Qt::LeftButton),
                                1 + mouseDelta * (bool)(event->buttons() & Qt::MidButton),
                                1 + mouseDelta * (bool)(event->buttons() & Qt::RightButton));
            break;
        }
        case VIEWPORT:
            if (event->buttons() & Qt::LeftButton) {
                *mViewportBottomRight = getWindowMouseCoordinate(event->x(), event->y());
            }
            break;
        case NONE:
        default:
            break;
    }

    update();
    mPreviousMouseX = newMouseX;
}

void Viewer::printMatrix(QMatrix4x4 matrix) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            std::cerr << matrix(i, j) << " ";
        }
        std::cerr << std::endl;
    }
}

// Drawing Functions
// ******************* Do Not Touch ************************ // 

void Viewer::draw_line(const QVector2D& p1, const QVector2D& p2) {

  const GLfloat lineVertices[] = {
    p1.x(), p1.y(), 1.0,
    p2.x(), p2.y(), 1.0
  };

  mVertexBufferObject.allocate(lineVertices, 3 * 2 * sizeof(float));

  glDrawArrays(GL_LINES, 0, 2);
}

void Viewer::set_colour(const QColor& col)
{
  mProgram.setUniformValue(mColorLocation, col.red(), col.green(), col.blue());
}

void Viewer::draw_init() {
    glClearColor(0.7, 0.7, 0.7, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, (double)width(), 0.0, (double)height());
    glViewport(0, 0, width(), height());

    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glLineWidth(1.0);
}
