#include <QtWidgets>
#include <iostream>
#include "AppWindow.hpp"

AppWindow::AppWindow() {
    setWindowTitle("488 Assignment Two");

    QGLFormat glFormat;
    glFormat.setVersion(3,3);
    glFormat.setProfile(QGLFormat::CoreProfile);
    glFormat.setSampleBuffers(true);

    QVBoxLayout *layout = new QVBoxLayout;
    m_viewer = new Viewer(glFormat, this);
    layout->addWidget(m_viewer);
    setCentralWidget(new QWidget);
    centralWidget()->setLayout(layout);

    createActions();
    createMenu();
    statusBar();
}

void AppWindow::createApplicationActions() {

    QAction* resetPositionAct = new QAction(tr("Reset Position"), this);
    m_app_actions.push_back(resetPositionAct);
    resetPositionAct->setShortcut(QKeySequence(Qt::Key_I));
    connect(resetPositionAct, SIGNAL(triggered()), this, SLOT(resetPosition()));

    QAction* resetOrientationAct = new QAction(tr("Reset Orientation"), this);
    m_app_actions.push_back(resetOrientationAct);
    resetOrientationAct->setShortcut(QKeySequence(Qt::Key_O));
    connect(resetOrientationAct, SIGNAL(triggered()), this, SLOT(resetOrientation()));

    QAction* resetJointsAct = new QAction(tr("Reset Joints"), this);
    m_app_actions.push_back(resetJointsAct);
    resetJointsAct->setShortcut(QKeySequence(Qt::Key_N));
    connect(resetJointsAct, SIGNAL(triggered()), this, SLOT(resetJoints()));

    QAction* resetAllAct = new QAction(tr("Reset All"), this);
    m_app_actions.push_back(resetAllAct);
    resetAllAct->setShortcut(QKeySequence(Qt::Key_A));
    connect(resetAllAct, SIGNAL(triggered()), this, SLOT(resetAll()));

    // Creates a new action for quiting and pushes it onto the menu actions vector 
    QAction* quitAct = new QAction(tr("&Quit"), this);
    m_app_actions.push_back(quitAct);
    // We set the accelerator keys
    // Alternatively, you could use: setShortcuts(Qt::CTRL + Qt::Key_P); 
    quitAct->setShortcuts(QKeySequence::Quit);
    // Set the tip
    quitAct->setStatusTip(tr("Exits the file"));
    // Connect the action with the signal and slot designated
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));
}

void AppWindow::createModeActions() {

    QAction* positionOrientationAct = new QAction(tr("Position/Orientation"), this);
    m_mode_actions.push_back(positionOrientationAct);
    positionOrientationAct->setShortcut(QKeySequence(Qt::Key_P));
    connect(positionOrientationAct, SIGNAL(triggered()), this, SLOT(setModePositionOrientation()));

    QAction* jointsAct = new QAction(tr("Joints"), this);
    m_mode_actions.push_back(jointsAct);
    jointsAct->setShortcut(QKeySequence(Qt::Key_J));
    connect(jointsAct, SIGNAL(triggered()), this, SLOT(setModeJoints()));
}

void AppWindow::createEditActions() {

    QAction* undoAct = new QAction(tr("Undo"), this);
    m_edit_actions.push_back(undoAct);
    undoAct->setShortcut(QKeySequence(Qt::Key_U));
    connect(undoAct, SIGNAL(triggered()), this, SLOT(undo()));

    QAction* redoAct = new QAction(tr("Redo"), this);
    m_edit_actions.push_back(redoAct);
    redoAct->setShortcut(QKeySequence(Qt::Key_R));
    connect(redoAct, SIGNAL(triggered()), this, SLOT(redo()));
}

void AppWindow::createOptionActions() {

    QAction* circleAct = new QAction(tr("Circle"), this);
    m_options_actions.push_back(circleAct);
    circleAct->setShortcut(QKeySequence(Qt::Key_C));
    connect(circleAct, SIGNAL(triggered()), this, SLOT(toggleCircle()));

    QAction* zBufferAct = new QAction(tr("Z-Buffer"), this);
    m_options_actions.push_back(zBufferAct);
    zBufferAct->setShortcut(QKeySequence(Qt::Key_Z));
    connect(zBufferAct, SIGNAL(triggered()), this, SLOT(toggleZBuffer()));

    QAction* backfaceAct = new QAction(tr("Backface Cull"), this);
    m_options_actions.push_back(backfaceAct);
    backfaceAct->setShortcut(QKeySequence(Qt::Key_B));
    connect(backfaceAct, SIGNAL(triggered()), this, SLOT(toggleBackfaceCulling()));

    QAction* frontfaceAct = new QAction(tr("Frontface Cull"), this);
    m_options_actions.push_back(frontfaceAct);
    frontfaceAct->setShortcut(QKeySequence(Qt::Key_F));
    connect(frontfaceAct, SIGNAL(triggered()), this, SLOT(toggleFrontfaceCulling()));
}

void AppWindow::createActions() {
    createApplicationActions();
    createModeActions();
    createEditActions();
    createOptionActions();
}

void AppWindow::createMenu() {
    m_menu_app = menuBar()->addMenu(tr("&Application"));
    m_menu_mode = menuBar()->addMenu(tr("Mode"));
    m_menu_edit = menuBar()->addMenu(tr("Edit"));
    m_menu_options = menuBar()->addMenu(tr("Options"));

    for (auto& action : m_app_actions) {
        m_menu_app->addAction(action);
    }

    QActionGroup* modeGroup = new QActionGroup(this);
    for (auto& action : m_mode_actions) {
        action->setActionGroup(modeGroup);
        action->setCheckable(true);
        m_menu_mode->addAction(action);
    }
    m_mode_actions[0]->trigger();

    for (auto& action : m_edit_actions) {
        m_menu_edit->addAction(action);
    }

    for (auto& action : m_options_actions) {
        action->setCheckable(true);
        m_menu_options->addAction(action);
    }
}

void AppWindow::setSceneNode(SceneNode* node) {
    mSceneNode = node;
    m_viewer->setSceneNode(node);
}

void AppWindow::resetPosition() {
    m_viewer->resetPosition();
}

void AppWindow::resetOrientation() {
    m_viewer->resetOrientation();
}

void AppWindow::resetJoints() {
    m_viewer->resetJoints();
}

void AppWindow::resetAll() {
    resetPosition();
    resetOrientation();
    resetJoints();
}

void AppWindow::setModePositionOrientation() {
    m_viewer->setMode(Viewer::POSITION_ORIENTATION);
}

void AppWindow::setModeJoints() {
    m_viewer->setMode(Viewer::JOINTS);
}

void AppWindow::undo() {
    if (!m_viewer->undo()) {
        statusBar()->showMessage(tr("Undo Failed"), 1000);
    }
}

void AppWindow::redo() {
    if (!m_viewer->redo()) {
        statusBar()->showMessage(tr("Redo Failed"), 1000);
    }
}


void AppWindow::toggleCircle() {
    m_viewer->setDrawCircle(!m_viewer->isDrawingCircle());
}

void AppWindow::toggleZBuffer() {
    m_viewer->setUseZBuffer(!m_viewer->isUsingZBuffer());
}

void AppWindow::toggleBackfaceCulling() {
    m_viewer->setUseBackfaceCull(!m_viewer->isUsingBackfaceCull());
}

void AppWindow::toggleFrontfaceCulling() {
    m_viewer->setUseFrontfaceCull(!m_viewer->isUsingFrontfaceCull());
}
