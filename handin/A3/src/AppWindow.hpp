#ifndef APPWINDOW_HPP
#define APPWINDOW_HPP

#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <vector>
#include "Viewer.hpp"
#include "scene.hpp"

class AppWindow : public QMainWindow
{
    Q_OBJECT

public:
    AppWindow();
    void setSceneNode(SceneNode* node);

private slots:

    void resetPosition();
    void resetOrientation();
    void resetJoints();
    void resetAll();

    void setModePositionOrientation();
    void setModeJoints();

    void undo();
    void redo();

    void toggleZBuffer();
    void toggleBackfaceCulling();
    void toggleFrontfaceCulling();
    
    void toggleCircle();

private:
    void createApplicationActions();
    void createModeActions();
    void createEditActions();
    void createOptionActions();
    void createActions();
    void createMenu();

    // Each menu itself
    QMenu* m_menu_app;
    QMenu* m_menu_mode;
    QMenu* m_menu_edit;
    QMenu* m_menu_options;

    std::vector<QAction*> m_app_actions;
    std::vector<QAction*> m_mode_actions;
    std::vector<QAction*> m_edit_actions;
    std::vector<QAction*> m_options_actions;

    SceneNode* mSceneNode;

    Viewer* m_viewer;
};

#endif
