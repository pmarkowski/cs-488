#include <QtWidgets>
#include <QtOpenGL>
#include "Viewer.hpp"
#include <iostream>
#include <math.h>
#include <GL/glu.h>
#include "primitive.hpp"
#include "trackball.hpp"

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE 0x809D
#endif

Viewer::Viewer(const QGLFormat& format, QWidget *parent) 
    : QGLWidget(format, parent) 
#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    , mCircleBufferObject(QOpenGLBuffer::VertexBuffer)
    , mSphereBufferObject(QOpenGLBuffer::VertexBuffer)
    , mVertexArrayObject(this)
#else 
    , mCircleBufferObject(QGLBuffer::VertexBuffer)
    , mSphereBufferObject(QGLBuffer::VertexBuffer)
#endif
{
    drawCircle = false;
    setUseZBuffer(false);
    setUseBackfaceCull(false);
    setUseFrontfaceCull(false);
}

Viewer::~Viewer() {
    // Nothing to do here right now.
}

QSize Viewer::minimumSizeHint() const {
    return QSize(50, 50);
}

QSize Viewer::sizeHint() const {
    return QSize(300, 300);
}

void Viewer::initializeGL() {
    QGLFormat glFormat = QGLWidget::format();
    if (!glFormat.sampleBuffers()) {
        std::cerr << "Could not enable sample buffers." << std::endl;
        return;
    }

    glShadeModel(GL_SMOOTH);
    glClearColor( 0.4, 0.4, 0.4, 0.0 );
    
    if (!mProgram.addShaderFromSourceFile(QGLShader::Vertex, "shader.vert")) {
        std::cerr << "Cannot load vertex shader." << std::endl;
        return;
    }

    if (!mProgram.addShaderFromSourceFile(QGLShader::Fragment, "shader.frag")) {
        std::cerr << "Cannot load fragment shader." << std::endl;
        return;
    }

    if ( !mProgram.link() ) {
        std::cerr << "Cannot link shaders." << std::endl;
        return;
    }

    if (!mPickerProgram.addShaderFromSourceFile(QGLShader::Vertex, "shader.vert")) {
        std::cerr << "Cannot load vertex shader." << std::endl;
        return;
    }

    if (!mPickerProgram.addShaderFromSourceFile(QGLShader::Fragment, "shader_picker.frag")) {
        std::cerr << "Cannot load fragment shader." << std::endl;
        return;
    }

    if ( !mPickerProgram.link() ) {
        std::cerr << "Cannot link shaders." << std::endl;
        return;
    }

    float circleData[120];

    double radius = 1;
        
    for(size_t i=0; i<40; ++i) {
        circleData[i*3] = radius * cos(i*2*M_PI/40);
        circleData[i*3 + 1] = radius * sin(i*2*M_PI/40);
        circleData[i*3 + 2] = 0.0;
    }

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    mVertexArrayObject.create();
    mVertexArrayObject.bind();

    mCircleBufferObject.create();
    mCircleBufferObject.setUsagePattern(QOpenGLBuffer::StaticDraw);

    mSphereBufferObject.create();
    mSphereBufferObject.setUsagePattern(QOpenGLBuffer::StaticDraw);
#else 
    /*
     * if qt version is less than 5.1, use the following commented code
     * instead of QOpenGLVertexVufferObject. Also use QGLBuffer instead of 
     * QOpenGLBuffer.
     */
    uint vao;
     
    typedef void (APIENTRY *_glGenVertexArrays) (GLsizei, GLuint*);
    typedef void (APIENTRY *_glBindVertexArray) (GLuint);
     
    _glGenVertexArrays glGenVertexArrays;
    _glBindVertexArray glBindVertexArray;
     
    glGenVertexArrays = (_glGenVertexArrays) QGLWidget::context()->getProcAddress("glGenVertexArrays");
    glBindVertexArray = (_glBindVertexArray) QGLWidget::context()->getProcAddress("glBindVertexArray");
     
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);    

    mCircleBufferObject.create();
    mCircleBufferObject.setUsagePattern(QGLBuffer::StaticDraw);

    mSphereBufferObject.create();
    mSphereBufferObject.setUsagePattern(QGLBuffer::StaticDraw);
#endif


    mProgram.bind();

    if (!mCircleBufferObject.bind()) {

        mProgram.enableAttributeArray("vert");
        mProgram.setAttributeBuffer("vert", GL_FLOAT, 0, 3);
        std::cerr << "could not bind vertex buffer to the context." << std::endl;
        return;
    }
    mCircleBufferObject.allocate(circleData, 40 * 3 * sizeof(float));
    mCircleBufferObject.release();

    if (!mSphereBufferObject.bind()) {
        mProgram.enableAttributeArray("vert");
        mProgram.setAttributeBuffer("vert", GL_FLOAT, 0, 3);
        std::cerr << "could not bind vertex buffer to the context." << std::endl;
        return;
    }
    std::vector<float> sphereData = Sphere::get_vertex_vector();
    sphereVertices = sphereData.size();
    mSphereBufferObject.allocate(sphereData.data(), sphereVertices * sizeof(float));
    mSphereBufferObject.release();

    // Set up uniforms
    mCameraMatrixLocation = mProgram.uniformLocation("vpMatrix");
    mModelMatrixLocation = mProgram.uniformLocation("mMatrix");
    mLightPosLocation = mProgram.uniformLocation("light_pos");
    mEyePosLocation = mProgram.uniformLocation("eye_pos");
    mDiffuseLocation = mProgram.uniformLocation("k_diffuse");
    mSpecularLocation = mProgram.uniformLocation("k_specular");
    mPhongLocation = mProgram.uniformLocation("phong_coefficient");

    mProgram.release();

    mPickerProgram.bind();

    mPickerCameraMatrixLocation = mPickerProgram.uniformLocation("vpMatrix");
    mPickerModelMatrixLocation = mPickerProgram.uniformLocation("mMatrix");
    mColorLocation = mPickerProgram.uniformLocation("frag_color");

    mPickerProgram.release();
}

void Viewer::paintGL() {

    mProgram.bind();

    // Clear framebuffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Draw stuff
    setCameraMatrix(getCameraMatrix());
    mSceneNode->walk_gl(this, mOrientationMatrix);

    mProgram.release();

    if (drawCircle) {
        draw_trackball_circle();
    }
}

void Viewer::resizeGL(int width, int height) {
    if (height == 0) {
        height = 1;
    }

    mPerspMatrix.setToIdentity();
    mPerspMatrix.perspective(60.0, (float) width / (float) height, 0.001, 1000);

    glViewport(0, 0, width, height);
}

void Viewer::mousePressEvent ( QMouseEvent * event ) {
    mMousePrevY = event->y();
    mMousePrevX = event->x();
}

void Viewer::mouseReleaseEvent ( QMouseEvent * event ) {
    if (mMode == JOINTS) {
        if (event->button() == Qt::LeftButton) {
            
            // Render world with picking
            mPickerProgram.bind();

            // Clear framebuffer
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            setCameraMatrix(getCameraMatrix());

            mSceneNode->walk_gl(this, mOrientationMatrix, true);

            // get colour picked
            unsigned char pixelRgb[3];
            glReadPixels(event->x(), height() - event->y(), 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixelRgb);

            // walk down scene graph and retrieve the joint we got
            JointNode* pickedJoint = mSceneNode->retrieveJoint(QColor((unsigned int)pixelRgb[0], (unsigned int)pixelRgb[1], (unsigned int)pixelRgb[2]));

            if (pickedJoint != NULL) {
                bool pickedNodeBefore = false;
                int eraseNodeIndex = 0;
                for (int i = 0; i < mSelectedJointNodes.size(); ++i) {
                    if (mSelectedJointNodes[i] == pickedJoint) {
                        pickedNodeBefore = true;
                        eraseNodeIndex = i;
                    }
                }
                // if we already have this joint
                if (pickedNodeBefore) {
                    // remove it from 'selected joints' vector and unselect the material
                    mSelectedJointNodes.erase(mSelectedJointNodes.begin() + eraseNodeIndex);
                } else {
                    // add it to 'selected joints' vector and select the material
                    mSelectedJointNodes.push_back(pickedJoint);
                }
                pickedJoint->picked();
            }

            mPickerProgram.release();

            // Redraw the scene
            update();
        } else {
            // put the transformation just made on undo stack
            mUndoStack.push_back(JointTransformation(mSceneNode));
            // Clear redo stack
            mRedoStack.clear();
        }
    }
}

void Viewer::mouseMoveEvent ( QMouseEvent * event ) {
    int mouseDeltaX = event->x() - mMousePrevX;
    int mouseDeltaY = event->y() - mMousePrevY;

    float translateSensitivity = 0.2;

    switch(mMode) {
        case POSITION_ORIENTATION: {
            if (event->buttons() & Qt::LeftButton) {
                translateWorld(mouseDeltaX * translateSensitivity, -mouseDeltaY * translateSensitivity, 0);
            }
            if (event->buttons() & Qt::MidButton) {
                translateWorld(0, 0, mouseDeltaY * translateSensitivity);
            }
            if (event->buttons() & Qt::RightButton) {
                float fDiameter = getTrackballRadius() * 2;
                float iCenterX, iCenterY, fNewModX, fNewModY, fOldModX, fOldModY;

                iCenterX = width() / 2.0;
                iCenterY = height() / 2.0;
                fOldModX = mMousePrevX - iCenterX;
                fOldModY = mMousePrevY - iCenterY;
                fNewModX = event->x() - iCenterX;
                fNewModY = event->y() - iCenterY;

                QVector3D rotVector = vCalcRotVec(fNewModX, fNewModY, fOldModX, fOldModY, fDiameter);
                rotVector = QVector3D(rotVector.x(), -rotVector.y(), rotVector.z());
                mOrientationMatrix *= vAxisRotMatrix(rotVector);
            }
            break;
        }
        case JOINTS: {
            if (event->buttons() & Qt::MidButton) {
                for (JointNode* joint : mSelectedJointNodes) {
                    joint->rotate_joint_x(mouseDeltaY);
                }
            }
            if (event->buttons() & Qt::RightButton) {
                for (JointNode* joint : mSelectedJointNodes) {
                    joint->rotate_joint_y(mouseDeltaX);
                }
            }
            break;
        }
        default:
            break;
    }

    mMousePrevX = event->x();
    mMousePrevY = event->y();

    update();
}

QMatrix4x4 Viewer::getCameraMatrix() {
    // Todo: Ask if we want to keep this.
    QMatrix4x4 vMatrix;

    QMatrix4x4 cameraTransformation;
    QVector3D cameraPosition = cameraTransformation * QVector3D(0, 0, 20.0);
    QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 1, 0);

    vMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);

    mProgram.setUniformValue(mLightPosLocation, cameraPosition);
    mProgram.setUniformValue(mEyePosLocation, cameraPosition);

    return mPerspMatrix * vMatrix * mTransformMatrix;
}

void Viewer::translateWorld(float x, float y, float z) {
    // Todo: Ask if we want to keep this.
    mTransformMatrix.translate(x, y, z);
}

void Viewer::rotateWorld(float x, float y, float z) {
    // Todo: Ask if we want to keep this.
    mTransformMatrix.rotate(x, y, z);
}

void Viewer::scaleWorld(float x, float y, float z) {
    // Todo: Ask if we want to keep this.
    mTransformMatrix.scale(x, y, z);
}

void Viewer::setPhongMaterial(QColor diffuse, QColor specular, double p) {
    mProgram.setUniformValue(mDiffuseLocation, diffuse.redF(), diffuse.greenF(), diffuse.blueF());
    mProgram.setUniformValue(mSpecularLocation, specular.redF(), specular.greenF(), specular.blueF());
    mProgram.setUniformValue(mPhongLocation, (float)p);
}

void Viewer::set_colour(const QColor& col)
{
  mPickerProgram.setUniformValue(mColorLocation, col.redF(), col.greenF(), col.blueF());
}

void Viewer::setCameraMatrix(QMatrix4x4 matrix) {
    mProgram.setUniformValue(mCameraMatrixLocation, matrix);
    mPickerProgram.setUniformValue(mPickerCameraMatrixLocation, matrix);
}

void Viewer::setModelMatrix(QMatrix4x4 matrix) {
    mProgram.setUniformValue(mModelMatrixLocation, matrix);
    mPickerProgram.setUniformValue(mPickerModelMatrixLocation, matrix);
}

void Viewer::drawSphere() {
    mSphereBufferObject.bind();

    mProgram.enableAttributeArray("vert");
    mProgram.setAttributeBuffer("vert", GL_FLOAT, 0, 3);

    mPickerProgram.enableAttributeArray("vert");
    mPickerProgram.setAttributeBuffer("vert", GL_FLOAT, 0, 3);

    glDrawArrays(GL_TRIANGLES, 0, sphereVertices);

    mSphereBufferObject.release();
}

void Viewer::draw_trackball_circle()
{
    int current_width = width();
    int current_height = height();

    // Set up for orthogonal drawing to draw a circle on screen.
    // You'll want to make the rest of the function conditional on
    // whether or not we want to draw the circle this time around.

    mPickerProgram.bind();

    set_colour(QColor(0.0, 0.0, 0.0));

    // Bind buffer object
    mCircleBufferObject.bind();

    mPickerProgram.enableAttributeArray("vert");
    mPickerProgram.setAttributeBuffer("vert", GL_FLOAT, 0, 3);

    // Set orthographic Matrix
    QMatrix4x4 orthoMatrix;
    orthoMatrix.ortho(0.0, (float)current_width, 
           0.0, (float)current_height, -0.1, 0.1);
    setCameraMatrix(orthoMatrix);

    // Translate the view to the middle
    QMatrix4x4 transformMatrix;
    transformMatrix.translate(width()/2.0, height()/2.0, 0.0);
    float radius = getTrackballRadius();
    transformMatrix.scale(radius, radius, 0.0);
    setModelMatrix(transformMatrix);

    // Draw buffer
    glDrawArrays(GL_LINE_LOOP, 0, 40);

    mCircleBufferObject.release();

    mPickerProgram.release();
}

float Viewer::getTrackballRadius() {
    return std::fmin(width() / 4.0, height() / 4.0);
}

void Viewer::setSceneNode(SceneNode* node) {
    mSceneNode = node;
    mUndoStack.push_back(JointTransformation(mSceneNode));
}

bool Viewer::isUsingZBuffer() {
    return useZBuffer;
}

bool Viewer::isUsingBackfaceCull() {
    return useBackfaceCull;
}

bool Viewer::isUsingFrontfaceCull() {
    return useFrontfaceCull;
}

bool Viewer::isDrawingCircle() {
    return drawCircle;
}

void Viewer::setUseZBuffer(bool useZBuffer) {
    this->useZBuffer = useZBuffer;
    if (useZBuffer) {
        glEnable(GL_DEPTH_TEST);
    } else {
        glDisable(GL_DEPTH_TEST);
    }
    update();
}

void Viewer::setUseBackfaceCull(bool useBackfaceCull) {
    this->useBackfaceCull = useBackfaceCull;

    if (useBackfaceCull) {
        glEnable(GL_CULL_FACE);
        if (useFrontfaceCull) {
            glCullFace(GL_FRONT_AND_BACK);
        } else {
            glCullFace(GL_BACK);
        }
    } else {
        if (!useFrontfaceCull) {
            glDisable(GL_CULL_FACE);
        }
    }

    update();
}

void Viewer::setUseFrontfaceCull(bool useFrontfaceCull) {
    this->useFrontfaceCull = useFrontfaceCull;

    if (useFrontfaceCull) {
        glEnable(GL_CULL_FACE);
        if (useBackfaceCull) {
            glCullFace(GL_FRONT_AND_BACK);
        } else {
            glCullFace(GL_FRONT);
        }
    } else {
        if (!useBackfaceCull) {
            glDisable(GL_CULL_FACE);
        }
    }

    update();
}


void Viewer::setDrawCircle(bool drawCircle) {
    this->drawCircle = drawCircle;
    update();
}

void Viewer::setMode(Mode mode) {
    mMode = mode;
}

void Viewer::resetPosition() {
    mTransformMatrix.setToIdentity();
    update();
}

void Viewer::resetOrientation() {
    mOrientationMatrix.setToIdentity();
    update();
}

void Viewer::resetJoints() {
    // undo until undo stack is empty
    while (!mUndoStack.empty()) {
        undo();
    }

    mUndoStack.clear();
    mRedoStack.clear();
}

bool Viewer::undo() {
    // pop off undo stack and onto redo stack
    if (mUndoStack.size() <= 1) {
        return false;
    }

    JointTransformation transformation = mUndoStack.back();
    mUndoStack.pop_back();

    // save current state on redo stack
    mRedoStack.push_back(transformation);

    // pass colour ids and rotations to mSceneNode which will do it
    mSceneNode->performJointRotations(mUndoStack.back().get_colourIDs(), mUndoStack.back().get_rotationXs(), mUndoStack.back().get_rotationYs());

    update();

    return true;
}

bool Viewer::redo() {
    // pop off redo stack and onto undo stack
    if (mRedoStack.empty()) {
        return false;
    }
    
    JointTransformation transformation = mRedoStack.back();
    mRedoStack.pop_back();
    mUndoStack.push_back(transformation);

    // pass colour ids and rotations to mSceneNode which will do it
    mSceneNode->performJointRotations(transformation.get_colourIDs(), transformation.get_rotationXs(), transformation.get_rotationYs());

    update();

    return true;
}
