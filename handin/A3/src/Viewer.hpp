#ifndef CS488_VIEWER_HPP
#define CS488_VIEWER_HPP

#include <QGLWidget>
#include <QGLShaderProgram>
#include <QMatrix4x4>
#include <QtGlobal>

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#else 
#include <QGLBuffer>
#endif

#include "scene.hpp"
#include "jointTransformation.hpp"

class Viewer : public QGLWidget {
    
    Q_OBJECT

public:
    Viewer(const QGLFormat& format, QWidget *parent = 0);
    virtual ~Viewer();
    
    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    void resetPosition();
    void resetOrientation();
    void resetJoints();

    void setUseZBuffer(bool useZBuffer);
    void setUseBackfaceCull(bool useBackfaceCull);
    void setUseFrontfaceCull(bool useFrontfaceCull);

    bool isUsingZBuffer();
    bool isUsingBackfaceCull();
    bool isUsingFrontfaceCull();

    void setSceneNode(SceneNode* node);

    void setPhongMaterial(QColor diffuse, QColor specular, double p);
    void set_colour(const QColor& col);

    void setDrawCircle(bool drawCircle);
    bool isDrawingCircle();

    void setModelMatrix(QMatrix4x4 matrix);

    void drawSphere();

    enum Mode {
        POSITION_ORIENTATION,
        JOINTS
    };

    void setMode(Mode mode);

    bool undo();
    bool redo();

    // If you want to render a new frame, call do not call paintGL(),
    // instead, call update() to ensure that the view gets a paint 
    // event.
  
protected:

    // Events we implement

    // Called when GL is first initialized
    virtual void initializeGL();
    // Called when our window needs to be redrawn
    virtual void paintGL();
    // Called when the window is resized (formerly on_configure_event)
    virtual void resizeGL(int width, int height);
    // Called when a mouse button is pressed
    virtual void mousePressEvent ( QMouseEvent * event );
    // Called when a mouse button is released
    virtual void mouseReleaseEvent ( QMouseEvent * event );
    // Called when the mouse moves
    virtual void mouseMoveEvent ( QMouseEvent * event );
    
    // Draw a circle for the trackball, with OpenGL commands.
    // Assumes the context for the viewer is active.
    void draw_trackball_circle();

private:

    float getTrackballRadius();
    QMatrix4x4 getCameraMatrix();
    void setCameraMatrix(QMatrix4x4 matrix);
    void translateWorld(float x, float y, float z);
    void rotateWorld(float x, float y, float z);
    void scaleWorld(float x, float y, float z);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    QOpenGLBuffer mCircleBufferObject;
    QOpenGLBuffer mSphereBufferObject;
    QOpenGLVertexArrayObject mVertexArrayObject;
#else 
    QGLBuffer mCircleBufferObject;
    QGLBuffer mSphereBufferObject;
#endif
    
    int mCameraMatrixLocation;
    int mModelMatrixLocation;
    int mLightPosLocation;
    int mEyePosLocation;
    int mDiffuseLocation;
    int mSpecularLocation;
    int mPhongLocation;

    int mPickerCameraMatrixLocation;
    int mPickerModelMatrixLocation;
    int mColorLocation;

    QMatrix4x4 mPerspMatrix;
    QMatrix4x4 mTransformMatrix;
    QMatrix4x4 mOrientationMatrix;
    QGLShaderProgram mProgram;
    QGLShaderProgram mPickerProgram;

    SceneNode* mSceneNode;
    std::vector<JointNode*> mSelectedJointNodes;

    std::vector<JointTransformation> mUndoStack;
    std::vector<JointTransformation> mRedoStack;

    int sphereVertices;

    bool drawCircle;

    bool useZBuffer;
    bool useBackfaceCull;
    bool useFrontfaceCull;

    Mode mMode;
    int mMousePrevY;
    int mMousePrevX;

    JointTransformation mCurrentJointStates;
};

#endif