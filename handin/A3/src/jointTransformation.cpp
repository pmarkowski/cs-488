#include "jointTransformation.hpp"

JointTransformation::JointTransformation() {};

JointTransformation::JointTransformation(SceneNode* scene)
{
	std::vector<JointNode*> joints = scene->getJoints();
	for (JointNode* joint : joints) {
		m_colourIDs.push_back(joint->get_colour());
		m_netRotationXs.push_back(joint->get_x_rotation());
		m_netRotationYs.push_back(joint->get_y_rotation());
	}
}

JointTransformation::~JointTransformation() {

}
