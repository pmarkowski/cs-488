#ifndef JOINTTRANSFORMATION_HPP
#define JOINTTRANSFORMATION_HPP

#include <vector>
#include "scene.hpp"
#include "algebra.hpp"

class JointTransformation
{
public:
	JointTransformation();
	JointTransformation(SceneNode* scene);
	~JointTransformation();

	std::vector<Colour> get_colourIDs() { return m_colourIDs; };
	std::vector<double> get_rotationXs() { return m_netRotationXs; };
	std::vector<double> get_rotationYs() { return m_netRotationYs; };

private:
	std::vector<Colour> m_colourIDs;
	std::vector<double> m_netRotationXs;
	std::vector<double> m_netRotationYs;
};

#endif