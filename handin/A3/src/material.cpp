#include "Viewer.hpp"
#include "material.hpp"

Material::~Material()
{
}

PhongMaterial::PhongMaterial(const Colour& kd, const Colour& ks, double shininess)
  : m_kd(kd), m_ks(ks), m_shininess(shininess)
{
}

PhongMaterial::~PhongMaterial()
{
}

PhongMaterial* PhongMaterial::invertedMaterial() {
	Colour invertedKd = m_kd;
	invertedKd.setRgbF(1.0 - m_kd.redF(), 1.0 - m_kd.greenF(), 1.0 - m_kd.blueF());
	return new PhongMaterial(invertedKd, m_ks, m_shininess);
}

void PhongMaterial::apply_gl(Viewer* viewer) const
{
  // Perform OpenGL calls necessary to set up this material.
  viewer->setPhongMaterial(m_kd, m_ks, m_shininess);
}