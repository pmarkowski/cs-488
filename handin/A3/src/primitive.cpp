#include <GL/glu.h>
#include "primitive.hpp"
#include <tuple>
#include "Viewer.hpp"

Primitive::~Primitive()
{
}

Sphere::~Sphere()
{
}

void Sphere::walk_gl(Viewer* viewer, bool picking) const
{
  viewer->drawSphere();
}

/*
 * Sphere algorithm adapted from Andreas Kahler's blog post
 * http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
 */

QVector3D getMiddlePoint(QVector3D point1, QVector3D point2) {

	QVector3D middle = QVector3D(
	    (point1.x() + point2.x()) / 2.0,
	    (point1.y() + point2.y()) / 2.0,
	    (point1.z() + point2.z()) / 2.0);

	return middle.normalized();
}

std::vector<float> Sphere::get_vertex_vector() {
  return get_vertex_vector(3);
}

std::vector<float> Sphere::get_vertex_vector(int recursionLevel) {

	std::vector<QVector3D> icosahedron_vertices;
	double t = (1.0 + sqrt(5.0)) / 2.0;

	icosahedron_vertices.push_back(QVector3D(-1,  t,  0).normalized());
	icosahedron_vertices.push_back(QVector3D( 1,  t,  0).normalized());
	icosahedron_vertices.push_back(QVector3D(-1, -t,  0).normalized());
	icosahedron_vertices.push_back(QVector3D( 1, -t,  0).normalized());

	icosahedron_vertices.push_back(QVector3D( 0, -1,  t).normalized());
	icosahedron_vertices.push_back(QVector3D( 0,  1,  t).normalized());
	icosahedron_vertices.push_back(QVector3D( 0, -1, -t).normalized());
	icosahedron_vertices.push_back(QVector3D( 0,  1, -t).normalized());

	icosahedron_vertices.push_back(QVector3D( t,  0, -1).normalized());
	icosahedron_vertices.push_back(QVector3D( t,  0,  1).normalized());
	icosahedron_vertices.push_back(QVector3D(-t,  0, -1).normalized());
	icosahedron_vertices.push_back(QVector3D(-t,  0,  1).normalized());

	std::vector<std::tuple<QVector3D, QVector3D, QVector3D>> faces;
	faces.push_back(std::make_tuple(icosahedron_vertices[0], icosahedron_vertices[11], icosahedron_vertices[5]));
	faces.push_back(std::make_tuple(icosahedron_vertices[0], icosahedron_vertices[5],  icosahedron_vertices[1]));
	faces.push_back(std::make_tuple(icosahedron_vertices[0], icosahedron_vertices[1],  icosahedron_vertices[7]));
	faces.push_back(std::make_tuple(icosahedron_vertices[0], icosahedron_vertices[7],  icosahedron_vertices[10]));
	faces.push_back(std::make_tuple(icosahedron_vertices[0], icosahedron_vertices[10], icosahedron_vertices[11]));

	faces.push_back(std::make_tuple(icosahedron_vertices[1], icosahedron_vertices[5],  icosahedron_vertices[9]));
	faces.push_back(std::make_tuple(icosahedron_vertices[5], icosahedron_vertices[11], icosahedron_vertices[4]));
	faces.push_back(std::make_tuple(icosahedron_vertices[11], icosahedron_vertices[10], icosahedron_vertices[2]));
	faces.push_back(std::make_tuple(icosahedron_vertices[10], icosahedron_vertices[7], icosahedron_vertices[6]));
	faces.push_back(std::make_tuple(icosahedron_vertices[7], icosahedron_vertices[1],  icosahedron_vertices[8]));

	faces.push_back(std::make_tuple(icosahedron_vertices[3], icosahedron_vertices[9],  icosahedron_vertices[4]));
	faces.push_back(std::make_tuple(icosahedron_vertices[3], icosahedron_vertices[4],  icosahedron_vertices[2]));
	faces.push_back(std::make_tuple(icosahedron_vertices[3], icosahedron_vertices[2],  icosahedron_vertices[6]));
	faces.push_back(std::make_tuple(icosahedron_vertices[3], icosahedron_vertices[6],  icosahedron_vertices[8]));
	faces.push_back(std::make_tuple(icosahedron_vertices[3], icosahedron_vertices[8],  icosahedron_vertices[9]));

	faces.push_back(std::make_tuple(icosahedron_vertices[4], icosahedron_vertices[9],  icosahedron_vertices[5]));
	faces.push_back(std::make_tuple(icosahedron_vertices[2], icosahedron_vertices[4],  icosahedron_vertices[11]));
	faces.push_back(std::make_tuple(icosahedron_vertices[6], icosahedron_vertices[2],  icosahedron_vertices[10]));
	faces.push_back(std::make_tuple(icosahedron_vertices[8], icosahedron_vertices[6],  icosahedron_vertices[7]));
	faces.push_back(std::make_tuple(icosahedron_vertices[9], icosahedron_vertices[8],  icosahedron_vertices[1]));

	// Refine Triangles
	for (int i = 0; i < recursionLevel; ++i)
	{
		std::vector<std::tuple<QVector3D, QVector3D, QVector3D>> faces2;

		for (auto tri : faces) {
			// TODO: def getMiddlePoint
			QVector3D a = getMiddlePoint(std::get<0>(tri), std::get<1>(tri));
			QVector3D b = getMiddlePoint(std::get<1>(tri), std::get<2>(tri));
			QVector3D c = getMiddlePoint(std::get<2>(tri), std::get<0>(tri));

			faces2.push_back(std::make_tuple(std::get<0>(tri), a, c));
			faces2.push_back(std::make_tuple(std::get<1>(tri), b, a));
			faces2.push_back(std::make_tuple(std::get<2>(tri), c, b));
			faces2.push_back(std::make_tuple(a, b, c));
		}

		faces = faces2;
	}

	std::vector<float> vertices;
	for (auto tri : faces) {
		QVector3D point1 = std::get<0>(tri);
		QVector3D point2 = std::get<1>(tri);
		QVector3D point3 = std::get<2>(tri);

		vertices.push_back(point1.x());
		vertices.push_back(point1.y());
		vertices.push_back(point1.z());

		vertices.push_back(point2.x());
		vertices.push_back(point2.y());
		vertices.push_back(point2.z());

		vertices.push_back(point3.x());
		vertices.push_back(point3.y());
		vertices.push_back(point3.z());
	}
	return vertices;
}
