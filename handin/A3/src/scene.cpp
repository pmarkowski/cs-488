#include "scene.hpp"
#include "Viewer.hpp"
#include <iostream>

unsigned char SceneNode::colourID[3] = {0, 0, 0};

SceneNode::SceneNode(const std::string& name)
  : m_name(name)
{
  m_colourID = Colour(colourID[0], colourID[1], colourID[2]);

  colourID[0]++;
  if (colourID[0] > 255) {
    colourID[0] = 0;
    colourID[1]++;
    if (colourID[1] > 255) {
      colourID[1] = 0;
      colourID[2]++;
    }
  }
}

SceneNode::~SceneNode()
{
}

void SceneNode::walk_gl(Viewer* viewer, QMatrix4x4 matrix, bool picking) const
{
  for (auto& child : m_children) {
    child->walk_gl(viewer, m_trans * matrix, picking);
  }
}

void SceneNode::rotate(char axis, double angle)
{
  switch(axis) {
    case 'x':
      m_trans.rotate(angle, 1, 0, 0);
      break;
    case 'y':
      m_trans.rotate(angle, 0, 1, 0);
      break;
    case 'z':
      m_trans.rotate(angle, 0, 0, 1);
      break;
    default:
      break;
  }
  set_transform(m_trans);
}

void SceneNode::scale(const Vector3D& amount)
{
  m_trans.scale(amount);
  set_transform(m_trans);
}

void SceneNode::translate(const Vector3D& amount)
{
  m_trans.translate(amount);
  set_transform(m_trans);
}

bool SceneNode::is_joint() const
{
  return false;
}

JointNode* SceneNode::retrieveJoint(Colour pickedColour) const {
  JointNode* pickedJoint = NULL;

  for (auto& child : m_children) {
    if (child->is_joint() && pickedJoint == NULL) {
      pickedJoint = child->retrieveJoint(pickedColour);
    }
  }

  return pickedJoint;
}

void SceneNode::picked() {
  
}

std::vector<JointNode*> SceneNode::getJoints() {
  std::vector<JointNode*> joints;

  if (is_joint()) {
    joints.push_back((JointNode*)this);
  }

  for (auto& child : m_children) {
    std::vector<JointNode*> childJoints = child->getJoints();
    joints.insert(joints.end(), childJoints.begin(), childJoints.end());
  }

  return joints;
}

void SceneNode::performJointRotations(std::vector<Colour> colourIDs, std::vector<double> rotationXs, std::vector<double> rotationYs) {
  for (int i = 0; i < colourIDs.size(); i++) {
    Colour colour = colourIDs[i];
    if (is_joint() && get_colour() == colour) {
      ((JointNode*)this)->set_x_rotation(rotationXs[i]);
      ((JointNode*)this)->set_y_rotation(rotationYs[i]);
    }
  }

  for (auto& child : m_children) {
    child->performJointRotations(colourIDs, rotationXs, rotationYs);
  }
}

JointNode::JointNode(const std::string& name)
  : SceneNode(name)
{
}

JointNode::~JointNode()
{
}

void JointNode::walk_gl(Viewer* viewer, QMatrix4x4 matrix, bool picking) const
{
  // Apply the joint rotation
  // X
  QMatrix4x4 new_m_trans = m_trans;
  new_m_trans.rotate(m_joint_x.init, 1, 0, 0);
  // Y
  new_m_trans.rotate(m_joint_y.init, 0, 1, 0);

  for (auto& child : m_children) {
    child->walk_gl(viewer, matrix * new_m_trans, picking);
  }
}

bool JointNode::is_joint() const
{
  return true;
}

JointNode* JointNode::retrieveJoint(Colour pickedColour) const {
  JointNode* pickedJoint = NULL;

  bool childPicked = false;
  for (auto& child : m_children) {
    if (!child->is_joint()) {
      childPicked |= (pickedColour == child->get_colour());
    }
  }

  if (childPicked) {
    return (JointNode*)this;
  }

  for (auto& child : m_children) {
    if (child->is_joint()) {
      pickedJoint = child->retrieveJoint(pickedColour);
    }
  }

  return pickedJoint;
}

void JointNode::picked() {
  for (auto& child : m_children) {
    if (!child->is_joint()) {
      child->picked();
    }
  }
}


void JointNode::set_joint_x(double min, double init, double max)
{
  m_joint_x.min = min;
  m_joint_x.init = init;
  m_joint_x.max = max;
}

void JointNode::set_joint_y(double min, double init, double max)
{
  m_joint_y.min = min;
  m_joint_y.init = init;
  m_joint_y.max = max;
}

void JointNode::set_x_rotation(double newAngle) {
  if (newAngle < m_joint_x.min) {
    newAngle = m_joint_x.min;
  } else if (newAngle > m_joint_x.max) {
    newAngle = m_joint_x.max;
  }
  m_joint_x.init = newAngle;
}

void JointNode::set_y_rotation(double newAngle) {
  if (newAngle < m_joint_y.min) {
    newAngle = m_joint_y.min;
  } else if (newAngle > m_joint_y.max) {
    newAngle = m_joint_y.max;
  }
  m_joint_y.init = newAngle;
}


void JointNode::rotate_joint_x(double angle) {
  set_x_rotation(m_joint_x.init + angle);
}

void JointNode::rotate_joint_y(double angle) {
  set_y_rotation(m_joint_y.init + angle);
}


GeometryNode::GeometryNode(const std::string& name, Primitive* primitive)
  : SceneNode(name),
    m_primitive(primitive)
{
}

GeometryNode::~GeometryNode()
{
  delete m_selected_material;
}

void GeometryNode::picked() {
  Material* temp = m_material;
  m_material = m_selected_material;
  m_selected_material = temp;
}

void GeometryNode::walk_gl(Viewer* viewer, QMatrix4x4 matrix, bool picking) const
{
  viewer->setModelMatrix(matrix * m_trans);
  if (!picking) {
    m_material->apply_gl(viewer);
  } else {
    viewer->set_colour(m_colourID);
  }
  m_primitive->walk_gl(viewer, picking);

  for (auto& child : m_children) {
    child->walk_gl(viewer, matrix * m_trans, picking);
  }
}
