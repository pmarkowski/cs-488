#ifndef SCENE_HPP
#define SCENE_HPP

#include <list>
#include "algebra.hpp"
#include "primitive.hpp"
#include "material.hpp"

class Viewer;
class JointNode;

class SceneNode {
public:
  SceneNode(const std::string& name);
  virtual ~SceneNode();

  virtual void walk_gl(Viewer* viewer, QMatrix4x4 matrix, bool picking = false) const;

  virtual JointNode* retrieveJoint(Colour pickedColour) const;

  const Matrix4x4& get_transform() const { return m_trans; }
  const Matrix4x4& get_inverse() const { return m_invtrans; }

  const Colour get_colour() { return m_colourID; }

  const std::string get_name() { return m_name; }

  virtual void picked();

  std::vector<JointNode*> getJoints();

  void performJointRotations(std::vector<Colour> colourIDs, std::vector<double> rotationX, std::vector<double> rotationY);
  
  void set_transform(const Matrix4x4& m)
  {
    m_trans = m;
    m_invtrans = m.inverted();
  }

  void set_transform(const Matrix4x4& m, const Matrix4x4& i)
  {
    m_trans = m;
    m_invtrans = i;
  }

  void add_child(SceneNode* child)
  {
    m_children.push_back(child);
  }

  void remove_child(SceneNode* child)
  {
    m_children.remove(child);
  }

  // Callbacks to be implemented.
  // These will be called from Lua.
  void rotate(char axis, double angle);
  void scale(const Vector3D& amount);
  void translate(const Vector3D& amount);

  // Returns true if and only if this node is a JointNode
  virtual bool is_joint() const;
  
protected:

  static unsigned char colourID[3];
  
  std::string m_name;

  Colour m_colourID;

  // Transformations
  Matrix4x4 m_trans;
  Matrix4x4 m_invtrans;

  // Hierarchy
  typedef std::list<SceneNode*> ChildList;
  ChildList m_children;
};

class JointNode : public SceneNode {
public:
  JointNode(const std::string& name);
  virtual ~JointNode();

  virtual void walk_gl(Viewer* viewer, QMatrix4x4 matrix, bool bicking = false) const;

  virtual JointNode* retrieveJoint(Colour pickedColour) const;

  virtual bool is_joint() const;

  virtual void picked();

  void set_joint_x(double min, double init, double max);
  void set_joint_y(double min, double init, double max);

  void set_x_rotation(double angle);
  void set_y_rotation(double angle);

  double get_x_rotation() { return m_joint_x.init; };
  double get_y_rotation() { return m_joint_y.init; };

  void rotate_joint_x(double angle);
  void rotate_joint_y(double angle);

  struct JointRange {
    double min, init, max;
  };

  
protected:

  JointRange m_joint_x, m_joint_y;
};

class GeometryNode : public SceneNode {
public:
  GeometryNode(const std::string& name,
               Primitive* primitive);
  virtual ~GeometryNode();

  virtual void walk_gl(Viewer* viewer, QMatrix4x4 matrix, bool picking = false) const;

  const Material* get_material() const { return m_material; };

  virtual void picked();

  void set_material(Material* material)
  {
    m_material = material;
    m_selected_material = m_material->invertedMaterial();
  }

protected:
  Material* m_selected_material;
  Material* m_material;
  Primitive* m_primitive;
};

#endif
