#version 330

in vec3 fragVert;
in vec3 fragNormal;

uniform vec3 frag_color;

uniform mat4 mMatrix;

uniform vec3 light_pos;
uniform vec3 eye_pos;

uniform vec3 k_diffuse;
uniform vec3 k_specular;
uniform float phong_coefficient;

out vec4 finalColor;

void main()
{
    mat3 normalMatrix = transpose(inverse(mat3(mMatrix)));
    vec3 normal = normalize(normalMatrix * fragNormal);
    
    //calculate the location of this fragment (pixel) in world coordinates
    vec3 fragPosition = vec3(mMatrix * vec4(fragVert, 1));
    
    //calculate the vector from this pixels surface to the light source
    vec3 surfaceToLight = normalize(light_pos - fragPosition);

    //Diffuse
    vec3 diffuse = k_diffuse * clamp(dot(surfaceToLight, normal), 0.0, 1.0);

    //Specular
    vec3 v = normalize(eye_pos - fragPosition);
    vec3 r = normalize(-1 * surfaceToLight + 2 * dot(surfaceToLight, normal)*normal);
    vec3 specular = k_specular * pow(clamp(dot(r, v), 0.0, 1.0), phong_coefficient);

    finalColor = vec4(diffuse + specular, 1.0);
}
