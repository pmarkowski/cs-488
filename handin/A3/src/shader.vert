#version 330 

in vec3 vert;
uniform mat4 vpMatrix;
uniform mat4 mMatrix;

out vec3 fragVert;
out vec3 fragNormal;

void main()
{	
    gl_Position = vpMatrix * mMatrix * vec4(vert, 1.0);
    // gl_Position = ftransform();

    fragVert = vert;
    fragNormal = vert;
}
