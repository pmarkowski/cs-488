#ifndef TRACKBALL_H
#define TRACKBALL_H

#include "algebra.hpp"   /* For the definition of Matrix */

/* Function prototypes */
QVector3D vCalcRotVec(float fNewX, float fNewY,
                 float fOldX, float fOldY,
                 float fDiameter);

QMatrix4x4 vAxisRotMatrix(QVector3D rotVec);
QMatrix4x4 vAxisRotMatrix(float fVecX, float fVecY, float fVecZ);

#endif
