--
-- CS488 -- Introduction to Computer Graphics
-- 
-- a3mark.lua
--
-- A very simple scene creating a trivial puppet.  The TAs will be
-- using this scene as part of marking your assignment.  You'll
-- probably want to make sure you get reasonable results with it!

rootnode = gr.node('root')

skin = gr.material({1.0, 0.9, 0.79}, {0.1, 0.1, 0.1}, 10)
cloth = gr.material({0.0, 0.15, 0.58}, {0, 0, 0}, 0)
black_felt = gr.material({0.1, 0.1, 0.1}, {0, 0, 0}, 0)
mirror = gr.material({0.1,0.1,0.1}, {0,0,0}, 0, 0.9)
orange = gr.material({1.0, 0.5, 0.0}, {0.2, 0.1, 0.0}, 10)

s0 = gr.sphere('torso')
rootnode:add_child(s0)
s0:set_material(cloth)
s0:scale(1.0, 1.7, 0.7)

s1 = gr.sphere('pelvis')
rootnode:add_child(s1)
s1:translate(0.0, -1.7, 0.0)
s1:scale(1.5, 1.0, 0.7)
s1:set_material(cloth)

lHip = gr.joint('left hip', {-45.0, 0.0, 135}, {0.0, 0.0, 0.0})
rootnode:add_child(lHip)
lHip:translate(-1.5, -1.7, 0.0)

lThigh = gr.sphere('left thigh')
lHip:add_child(lThigh)
lThigh:scale(0.5, 1.7, 0.5)
lThigh:translate(0.0, -1.0, 0.0)
lThigh:set_material(cloth)

lKnee = gr.joint('left knee', {-60, 0.0, 0.0}, {0.0, 0.0, 0.0})
lHip:add_child(lKnee)
lKnee:translate(0.0, -1.7 * 2, 0)

lCalf = gr.sphere('left calf')
lKnee:add_child(lCalf)
lCalf:scale(0.4, 1.7, 0.5)
lCalf:translate(0.0, -1.0, 0.0)
lCalf:set_material(cloth)

lAnkle = gr.joint('left ankle', {-70, 0.0, 2.0}, {0.0, 0.0, 0.0})
lKnee:add_child(lAnkle)
lAnkle:translate(0, -1.7 * 2.0, 0)

lFoot = gr.sphere('left foot')
lAnkle:add_child(lFoot)
lFoot:scale(0.4, 0.3, 0.7)
lFoot:translate(0.0, -1.0, -0.5)
lFoot:set_material(cloth)

rHip = gr.joint('right hip', {-45.0, 0.0, 135}, {0.0, 0.0, 0.0})
rootnode:add_child(rHip)
rHip:translate(1.5, -1.7, 0.0)

rThigh = gr.sphere('right thigh')
rHip:add_child(rThigh)
rThigh:scale(0.5, 1.7, 0.5)
rThigh:translate(0.0, -1.0, 0.0)
rThigh:set_material(cloth)

rKnee = gr.joint('right knee', {-60, 0.0, 0.0}, {0.0, 0.0, 0.0})
rHip:add_child(rKnee)
rKnee:translate(0.0, -1.7 * 2, 0)

rCalf = gr.sphere('right calf')
rKnee:add_child(rCalf)
rCalf:scale(0.4, 1.7, 0.5)
rCalf:translate(0.0, -1.0, 0.0)
rCalf:set_material(cloth)

rAnkle = gr.joint('right ankle', {-70, 0.0, 2.0}, {0.0, 0.0, 0.0})
rKnee:add_child(rAnkle)
rAnkle:translate(0, -1.7 * 2.0, 0)

rFoot = gr.sphere('right foot')
rAnkle:add_child(rFoot)
rFoot:scale(0.4, 0.3, 0.7)
rFoot:translate(0.0, -1.0, -0.5)
rFoot:set_material(cloth)

s2 = gr.sphere('chest')
rootnode:add_child(s2)
s2:set_material(cloth)
s2:translate(0.0, 1.7, 0.0)
s2:scale(2.0, 1.0, 0.7)

bNeck = gr.joint('bottom of neck', {-10, 0, 40}, {0, 0, 0})
rootnode:add_child(bNeck)
bNeck:translate(0.0, 2.7, 0.0)

mNeck = gr.sphere('neck model')
bNeck:add_child(mNeck)
mNeck:scale(0.3, 0.5, 0.3)
mNeck:translate(0.0, 0.5, 0.0)
mNeck:set_material(skin)

tNeck = gr.joint('top of neck', {-10, 0, 30}, {-90, 0, 90})
bNeck:add_child(tNeck)
tNeck:translate(0, 0.25, 0)

head = gr.sphere('head')
tNeck:add_child(head)
head:scale(0.65, 1.0, 0.65)
head:translate(0.0, 1.0, 0.0)
head:set_material(skin)

require('cylinder')
cyl:set_material(black_felt)
tNeck:add_child(cyl)
cyl:translate(0.0, 1.75, 0.0)
cyl:scale(1.2, 0.1, 1.2)

cyl2 = gr.mesh('cylinder2', {
	{0.000000, 0.000000, 1.000000},
	{0.587785, 0.000000, 0.809017},
	{0.951057, 0.000000, 0.309017},
	{0.951057, 0.000000, -0.309017},
	{0.587785, 0.000000, -0.809017},
	{0.000000, 0.000000, -1.000000},
	{-0.587785, 0.000000, -0.809017},
	{-0.951057, 0.000000, -0.309017},
	{-0.951057, 0.000000, 0.309017},
	{-0.587785, 0.000000, 0.809017},
	{0.000000, 1.000000, 1.000000},
	{0.587785, 1.000000, 0.809017},
	{0.951057, 1.000000, 0.309017},
	{0.951057, 1.000000, -0.309017},
	{0.587785, 1.000000, -0.809017},
	{0.000000, 1.000000, -1.000000},
	{-0.587785, 1.000000, -0.809017},
	{-0.951057, 1.000000, -0.309017},
	{-0.951057, 1.000000, 0.309017},
	{-0.587785, 1.000000, 0.809017}
     }, {
	{0, 1, 11, 10},
	{1, 2, 12, 11},
	{2, 3, 13, 12},
	{3, 4, 14, 13},
	{4, 5, 15, 14},
	{5, 6, 16, 15},
	{6, 7, 17, 16},
	{7, 8, 18, 17},
	{8, 9, 19, 18},
	{9, 0, 10, 19},
	{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
	{10, 11, 12, 13, 14, 15, 16, 17, 18, 19}
     })
cyl2:set_material(black_felt)
tNeck:add_child(cyl2)
cyl2:translate(0.0, 1.75, 0.0)
cyl2:scale(0.6, 1.75, 0.6)

nose = gr.sphere('nose')
tNeck:add_child(nose)
nose:translate(0.0, 1.0, -0.65)
nose:scale(0.2, 0.2, 0.2)
nose:set_material(skin)

lShoulder = gr.joint('left shoulder', {-30.0, 20.0, 190.0}, {0.0, 0.0, 0.0})
rootnode:add_child(lShoulder)
lShoulder:translate(-2.0, 1.7, 0.0)

lBicep = gr.sphere('left bicep')
lShoulder:add_child(lBicep)
lBicep:scale(0.35, 1.7, 0.45)
lBicep:translate(0.0, -1.0, 0.0)
lBicep:set_material(cloth)

lElbow = gr.joint('left elbow', {0.0, 115.0, 170.0}, {0.0, 0.0, 0.0})
lShoulder:add_child(lElbow)
lElbow:translate(0.0, -1.7 * 2, 0.0)

lForearm = gr.sphere('left forearm')
lElbow:add_child(lForearm)
lForearm:scale(0.4, 1.4, 0.4)
lForearm:translate(0.0, -1.0, 0.0)
lForearm:set_material(cloth)

lWrist = gr.joint('left wrist', {-45.0, -45.0, 45.0}, {0.0, 0.0, 0.0})
lElbow:add_child(lWrist)
lWrist:translate(0.0, -1.4 * 2.0, 0.0)

lHand = gr.sphere('left hand')
lWrist:add_child(lHand)
lHand:scale(0.26, 0.4, 0.3)
lHand:translate(0.0, -1.0, 0.0)
lHand:set_material(skin)

rShoulder = gr.joint('right shoulder', {-30.0, 20.0, 190.0}, {0.0, 0.0, 0.0})
rootnode:add_child(rShoulder)
rShoulder:translate(2.0, 1.7, 0.0)

rBicep = gr.sphere('right bicep')
rShoulder:add_child(rBicep)
rBicep:scale(0.45, 1.7, 0.45)
rBicep:translate(0.0, -1.0, 0.0)
rBicep:set_material(cloth)

rElbow = gr.joint('right elbow', {0.0, 115.0, 170.0}, {0.0, 0.0, 0.0})
rShoulder:add_child(rElbow)
rElbow:translate(0.0, -1.7 * 2, 0.0)

rForearm = gr.sphere('right forearm')
rElbow:add_child(rForearm)
rForearm:scale(0.4, 1.4, 0.4)
rForearm:translate(0.0, -1.0, 0.0)
rForearm:set_material(cloth)

rWrist = gr.joint('right wrist', {-45.0, -45.0, 45.0}, {0.0, 0.0, 0.0})
rElbow:add_child(rWrist)
rWrist:translate(0.0, -1.4 * 2.0, 0.0)

rHand = gr.sphere('right hand')
rWrist:add_child(rHand)
rHand:scale(0.26, 0.4, 0.3)
rHand:translate(0.0, -1.0, 0.0)
rHand:set_material(skin)

lSphere = gr.sphere('mirror ball')
lWrist:add_child(lSphere)
lSphere:set_material(mirror)
lSphere:translate(2.0, -0.65, -1.20)
lSphere:scale(0.65, 0.65, 0.65)

rootnode:rotate('y', 180)

white_light = gr.light({-100.0, 150.0, 400.0}, {0.5, 0.5, 0.5}, {1, 0, 0})

gr.render(rootnode, 'puppet.png', 500, 500,
      {0, 2, 10}, {0, 0, -1}, {0, 1, 0}, 50,
      {0.2, 0.2, 0.2}, {white_light})