#include "a4.hpp"
#include "image.hpp"
#include <iomanip>

void printProgress(double progress) {
  std::cout << std::setprecision(2) << progress * 100 << "% complete..." << "\r";
}

void printComplete() {
  std::cout << "\033[J" << "Complete!" << std::endl;
}

double clamp(double n, double lower, double upper) {
  return std::fmax(lower, std::fmin(n, upper));
}

Matrix4x4 getViewToWorldMatrix(int width, int height, double fov, const Point3D& lookFrom, const Vector3D& viewDirection, const Vector3D& upVector) {

  double d = 1.0;

  // Translate
  Matrix4x4 T_1 = Matrix4x4(Vector4D(1, 0, 0,  -(double)width / 2.0),
                            Vector4D(0, 1, 0, -(double)height / 2.0),
                            Vector4D(0, 0, 1,                     d),
                            Vector4D(0, 0, 0,                     1));

  // Scale
  double h = 2.0 * d * tan((fov / 2.0) * (M_PI / 180.0));
  Matrix4x4 S_2 = Matrix4x4(Vector4D(-h / (double)height,                   0, 0, 0),
                            Vector4D(                  0, -h / (double)height, 0, 0),
                            Vector4D(                  0,                   0, 1, 0),
                            Vector4D(                  0,                   0, 0, 1));

  // Rotate
  Vector3D w = viewDirection;
  w.normalize();
  Vector3D u = upVector.cross(w);
  u.normalize();
  Vector3D v = w.cross(u);

  Matrix4x4 R_3 = Matrix4x4(Vector4D(u[0], v[0], w[0], 0),
                            Vector4D(u[1], v[1], w[1], 0),
                            Vector4D(u[2], v[2], w[2], 0),
                            Vector4D(   0,    0,    0, 1));

  // Translate again
  Matrix4x4 T_4 = Matrix4x4(Vector4D(1, 0, 0, lookFrom[0]),
                            Vector4D(0, 1, 0, lookFrom[1]),
                            Vector4D(0, 0, 1, lookFrom[2]),
                            Vector4D(0, 0, 0,           1));

  return T_4 * R_3 * S_2 * T_1;
}

Vector3D ggReflection(const Vector3D& v, const Vector3D& N) {
  return v - 2.0 * (v.dot(N)) * N;
}

Colour getBackground(Vector3D& rayDirection/* parameters that determine colour or whatever */) {
  // TODO: return a proper colour based on parameters
  return Colour(rayDirection[0], abs(rayDirection[1]), abs(rayDirection[2]));
}

Colour diffuseLight(Point3D& origin, Vector3D& N, SceneNode* scene, const std::list<Light*>& lights) {
  Colour netLight;

  for (Light* light : lights) {
    Vector3D surfaceToLight = light->position - origin;
    surfaceToLight.normalize();

    Vector3D N_intersect; double t, shininess, reflectivity_intersect; Colour kd, ks;
    if (!scene->intersection(origin, surfaceToLight, &N_intersect, &t, &kd, &ks, &shininess, &reflectivity_intersect)) {
      double brightness = clamp(
        N.dot(surfaceToLight) / (light->falloff[0] + light->falloff[1] * surfaceToLight.length() + light->falloff[2] * surfaceToLight.length() * surfaceToLight.length()),
        0.0,
        1.0);

      netLight = netLight + brightness * light->colour;
    }
  }

  return netLight;
}

Colour specularLight(Point3D& origin, Vector3D& N, Vector3D& eyeDirection, double shininess, SceneNode* scene, const std::list<Light*>& lights) {
  Colour netLight;

  for (Light* light : lights) {
    Vector3D surfaceToLight = light->position - origin;
    surfaceToLight.normalize();

    Vector3D N_intersect; double t, shininess_intersect, reflectivity_intersect; Colour kd, ks;
    if (!scene->intersection(origin, surfaceToLight, &N_intersect, &t, &kd, &ks, &shininess_intersect, &reflectivity_intersect)) {
      Vector3D reflection = 2.0 * (surfaceToLight.dot(N)) * N - surfaceToLight;

      double brightness = clamp(
        pow(reflection.dot(eyeDirection), shininess),
        0.0,
        1.0);

      netLight = netLight + brightness * light->colour;
    }
  }

  return netLight;
}

Colour traceRay(Point3D& rayOrigin, Vector3D& rayDirection, SceneNode* scene, const std::list<Light*>& lights, const Colour& ambient, bool recursive) {
  Vector3D N = Vector3D();
  double t = 0.0, shininess = 0.0, reflectivity = 0;
  Colour kd, ks;
  if (scene->intersection(rayOrigin, rayDirection, &N, &t, &kd, &ks, &shininess, &reflectivity)) {
    // Ambient lighting
    Colour colour = kd * ambient;

    Point3D x = rayOrigin + t * rayDirection;

    // Diffuse lighting
    if (kd != Colour(0)) {
      // diffuseLight is a function that sends a shadow ray towards lightsources.
      colour = colour + kd * diffuseLight(x, N, scene, lights);
    }

    // Specular lighting
    if (ks != Colour(0)) {
      Vector3D eyeDirection = -1.0 * rayDirection;
      colour = colour + ks * specularLight(x, N, eyeDirection, shininess, scene, lights);
    }

    // reflection?
    if (reflectivity != 0 && recursive) {
      Vector3D reflection = ggReflection(rayDirection, N);
      reflection.normalize();
      colour = colour + reflectivity * traceRay(x, reflection, scene, lights, ambient, false);
    }

    return colour;
  } else {
    return getBackground(rayDirection);
  }
}

void a4_render(// What to render
               SceneNode* root,
               // Where to output the image
               const std::string& filename,
               // Image size
               int width, int height,
               // Viewing parameters
               const Point3D& eye, const Vector3D& view,
               const Vector3D& up, double fov,
               // Lighting parameters
               const Colour& ambient,
               const std::list<Light*>& lights
               )
{
  // Fill in raytracing code here.

  std::cerr << "Stub: a4_render(" << root << ",\n     "
            << filename << ", " << width << ", " << height << ",\n     "
            << eye << ", " << view << ", " << up << ", " << fov << ",\n     "
            << ambient << ",\n     {";

  for (std::list<Light*>::const_iterator I = lights.begin(); I != lights.end(); ++I) {
    if (I != lights.begin()) {
      std::cerr << ", ";
    }
    std::cerr << **I;
  }
  std::cerr << "});" << std::endl;
  
  // For now, just make a sample image.

  Image img(width, height, 3);

  Matrix4x4 viewToWorld = getViewToWorldMatrix(width, height, fov, eye, view, up);

  unsigned int numPixels = height * width;

  for (int y = 0; y < height; y++) {
    printProgress((double)y * (double)width / (double)numPixels);
    for (int x = 0; x < width; x++) {

      // Get world space of x, y
      Point3D P_k = Point3D(x, y, 0);
      Point3D P_world = viewToWorld * P_k;

      // Get direction of ray
      // Note: ray originates at eye, looking in direction view
      Point3D rayOrigin = eye;
      Vector3D rayDirection = P_world - eye;
      rayDirection.normalize();

      // Shoot the ray into the scene and get back a colour
      Colour pixelColour = traceRay(rayOrigin, rayDirection, root, lights, ambient, REFLECTIONS);

      // Red:
      img(x, y, 0) = pixelColour.R();
      // Green:
      img(x, y, 1) = pixelColour.G();
      // Blue:
      img(x, y, 2) = pixelColour.B();
    }
  }

  img.savePng(filename);

  printComplete();
}
