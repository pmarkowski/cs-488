#include <iostream>
#include "scene_lua.hpp"
#include "a4.hpp"

bool DRAW_BOUNDING_BOXES;
bool REFLECTIONS;

int main(int argc, char** argv)
{
  std::string filename = "scene.lua";

  DRAW_BOUNDING_BOXES = false;
  REFLECTIONS = false;

  if (argc >= 2) {
    filename = argv[1];
  }
  if (argc >= 3) {
    std::string option = argv[2];
    if (option == "-b") {
        DRAW_BOUNDING_BOXES = true;
    } else if (option == "-r") {
      REFLECTIONS = true;
    }
  }
  if (argc >= 4) {
    std::string option = argv[3];
    if (option == "-b") {
        DRAW_BOUNDING_BOXES = true;
    } else if (option == "-r") {
      REFLECTIONS = true;
    }
  }

  if (!run_lua(filename)) {
    std::cerr << "Could not open " << filename << std::endl;
    return 1;
  }
}

