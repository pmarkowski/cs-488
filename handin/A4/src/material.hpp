#ifndef CS488_MATERIAL_HPP
#define CS488_MATERIAL_HPP

#include "algebra.hpp"

class Material {
public:
  virtual ~Material();
  virtual void apply_gl() const = 0;

  virtual Colour get_kd() = 0;
  virtual Colour get_ks() = 0;
  virtual double get_shininess() = 0;
  virtual double get_reflectivity() = 0;

protected:
  Material()
  {
  }
};

class PhongMaterial : public Material {
public:
  PhongMaterial(const Colour& kd, const Colour& ks, double shininess, double m_reflectivity = 0);
  virtual ~PhongMaterial();

  virtual Colour get_kd() { return m_kd; };
  virtual Colour get_ks() { return m_ks; };
  virtual double get_shininess() { return m_shininess; };
  virtual double get_reflectivity() { return m_reflectivity; };

  virtual void apply_gl() const;

private:
  Colour m_kd;
  Colour m_ks;

  double m_shininess;

  double m_reflectivity;
};


#endif
