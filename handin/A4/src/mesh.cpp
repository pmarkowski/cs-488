#include "mesh.hpp"
#include <iostream>

Mesh::Mesh(const std::vector<Point3D>& verts,
           const std::vector< std::vector<int> >& faces,
           bool drawBoundingBox)
  : m_verts(verts),
    m_faces(faces),
    m_drawBoundingBox(drawBoundingBox)
{
  // Get min and max X, Y, Z to create a bounding box
  double minX = INFINITY, maxX = -INFINITY, minY = INFINITY, maxY = -INFINITY, minZ = INFINITY, maxZ = -INFINITY;

  for (Point3D vertex : verts) {
    minX = fmin(minX, vertex[0]);
    maxX = fmax(maxX, vertex[0]);
    minY = fmin(minY, vertex[1]);
    maxY = fmax(maxY, vertex[1]);
    minZ = fmin(minZ, vertex[2]);
    maxZ = fmax(maxZ, vertex[2]);
  }

  double boundingBoxSize = fmax((maxX - minX), fmax((maxY - minY), (maxZ - minZ)));
  boundingBox = new NonhierBox(Point3D(minX, minY, minZ), boundingBoxSize);
}

std::vector<Point3D> Mesh::getFaceVerts(Face poly) {
  std::vector<Point3D> verts;
  for (int index : poly) {
    verts.push_back(m_verts[index]);
  }
  return verts;
}

bool Mesh::intersectionWithFace(Face poly, Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t) {
  std::vector<Point3D> polyVerts = getFaceVerts(poly);

  Vector3D polyNormal = (polyVerts[1] - polyVerts[0]).cross(polyVerts[2] - polyVerts[0]);
  polyNormal.normalize();

  double denominator = polyNormal.dot(rayDirection);
  if (denominator == 0) {
    return false;
  }

  double polyT = -(polyNormal.dot(rayOrigin - polyVerts[0])) / denominator;

  Point3D pointOnPlane = rayOrigin + polyT * rayDirection;

  bool pointInside = true;

  for (int i = 0; i < polyVerts.size(); ++i) {
    Vector3D polyLine = polyVerts[(i+1) % polyVerts.size()] - polyVerts[i];
    Vector3D testLine = polyNormal.cross(polyLine);

    pointInside = pointInside && ((pointOnPlane - polyVerts[i]).dot(testLine) >= 0);
  }

  // return point_is_inside
  if (pointInside) {
    *t = polyT;
    *N = polyNormal;
  }

  return pointInside;
}

bool Mesh::intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t) {
  // If we're drawing bounding boxes
  if (m_drawBoundingBox) {
    return boundingBox->intersection(rayOrigin, rayDirection, N, t);
  } else if (boundingBox->intersection(rayOrigin, rayDirection, N, t)) {
    // We've hit our bounding box, so let's check all of our faces for intersection
    bool hit = false;
    double t_old;
    Vector3D N_old;

    for (Face poly : m_faces) {
      double t_new;
      Vector3D N_new;

      bool hitFace = intersectionWithFace(poly, rayOrigin, rayDirection, &N_new, &t_new);

      if (hitFace && (t_new < t_old || !hit)) {
        hit = true;
        t_old = t_new;
        N_old = N_new;
      }
    }

    if (hit) {
      *t = t_old;
      *N = N_old;
    }

    return hit;
  }

  return false;
}

std::ostream& operator<<(std::ostream& out, const Mesh& mesh)
{
  std::cerr << "mesh({";
  for (std::vector<Point3D>::const_iterator I = mesh.m_verts.begin(); I != mesh.m_verts.end(); ++I) {
    if (I != mesh.m_verts.begin()) std::cerr << ",\n      ";
    std::cerr << *I;
  }
  std::cerr << "},\n\n     {";
  
  for (std::vector<Mesh::Face>::const_iterator I = mesh.m_faces.begin(); I != mesh.m_faces.end(); ++I) {
    if (I != mesh.m_faces.begin()) std::cerr << ",\n      ";
    std::cerr << "[";
    for (Mesh::Face::const_iterator J = I->begin(); J != I->end(); ++J) {
      if (J != I->begin()) std::cerr << ", ";
      std::cerr << *J;
    }
    std::cerr << "]";
  }
  std::cerr << "});" << std::endl;
  return out;
}
