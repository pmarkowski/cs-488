#ifndef CS488_MESH_HPP
#define CS488_MESH_HPP

#include <vector>
#include <iosfwd>
#include "primitive.hpp"
#include "algebra.hpp"

// A polygonal mesh.
class Mesh : public Primitive {
public:
  Mesh(const std::vector<Point3D>& verts,
       const std::vector< std::vector<int> >& faces,
       bool drawBoundingBox);
  virtual bool intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t);

  typedef std::vector<int> Face;
  
private:
  bool m_drawBoundingBox;

  std::vector<Point3D> m_verts;
  std::vector<Face> m_faces;

  NonhierBox* boundingBox;

  std::vector<Point3D> getFaceVerts(Face poly);
  bool intersectionWithFace(Face poly, Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t);

  friend std::ostream& operator<<(std::ostream& out, const Mesh& mesh);
};

#endif
