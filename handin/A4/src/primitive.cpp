#include "primitive.hpp"
#include "polyroots.hpp"

Primitive::~Primitive()
{
}

Sphere::~Sphere()
{
}

bool Sphere::intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t) {
    NonhierSphere collisionSphere = NonhierSphere(Point3D(0, 0, 0), 1.0);
    return collisionSphere.intersection(rayOrigin, rayDirection, N, t);
}

Cube::~Cube()
{
}

bool Cube::intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t) {
    NonhierBox collisionBox = NonhierBox(Point3D (0, 0, 0), 1.0);
    return collisionBox.intersection(rayOrigin, rayDirection, N, t);
}

NonhierSphere::~NonhierSphere()
{
}

bool NonhierSphere::intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t) {
    double A = rayDirection.dot(rayDirection);
    double B = 2 * rayDirection.dot(rayOrigin - m_pos);
    double C = (rayOrigin - m_pos).dot(rayOrigin - m_pos) - (m_radius * m_radius);

    // Solve quadratic formula
    double roots[2];
    size_t numRoots = quadraticRoots(A, B, C, roots);

    bool result = false;
    if (numRoots == 0) {
        result = false;
    } else if (numRoots == 1) {
        if (roots[0] > 0) {
            *t = roots[0];
            result = true;
        }
    } else if (numRoots == 2) {
        if (roots[0] > 0 && roots[1] > 0) {
            *t = fmin(roots[0], roots[1]);
            result = true;
        } else if (roots[0] > 0) {
            *t = roots[0];
            result = true;
        } else if (roots[1] > 0) {
            *t = roots[1];
            result = true;
        } else {
            result = false;
        }
    }

    // Calculate normal
    if (result) {
        Point3D hit = rayOrigin + (*t) * rayDirection;
        *N = Vector3D(hit[0] - m_pos[0], hit[1] - m_pos[1], hit[2] - m_pos[2]);
        N->normalize();
    }

    return result;
}

NonhierBox::~NonhierBox()
{
}

/*
 * based off of zacharmarz's code from: http://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms
 * used his since my original code resulted in errors
 */
bool NonhierBox::intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t) {

    Point3D lb = m_pos;
    Point3D rt = Point3D(lb[0] + m_size, lb[1] + m_size, lb[2] + m_size);
    // r.org is origin of ray
    double t1 = (lb[0] - rayOrigin[0]) / rayDirection[0];
    double t2 = (rt[0] - rayOrigin[0]) / rayDirection[0];
    double t3 = (lb[1] - rayOrigin[1]) / rayDirection[1];
    double t4 = (rt[1] - rayOrigin[1]) / rayDirection[1];
    double t5 = (lb[2] - rayOrigin[2]) / rayDirection[2];
    double t6 = (rt[2] - rayOrigin[2]) / rayDirection[2];

    double tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
    double tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

    // if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behind us
    // or if tmin > tmax, ray doesn't intersect AABB
    if (tmax < 0 || tmin > tmax)
    {
        return false;
    }

    Vector3D intersectionNormal;
    if (tmin == t1) {
        intersectionNormal = Vector3D(-1, 0, 0);
    } else if (tmin == t2) {
        intersectionNormal = Vector3D(1, 0, 0);
    } else if (tmin == t3) {
        intersectionNormal = Vector3D(0, 1, 0);
    } else if (tmin == t4) {
        intersectionNormal = Vector3D(0, -1, 0);
    } else if (tmin == t5) {
        intersectionNormal = Vector3D(0, 0, -1);
    } else if (tmin == t6) {
        intersectionNormal = Vector3D(0, 0, 1);
    }

    *t = tmin;
    *N = intersectionNormal;
    return true;
}
