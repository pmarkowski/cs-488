#ifndef CS488_PRIMITIVE_HPP
#define CS488_PRIMITIVE_HPP

#include "algebra.hpp"

class Primitive {
public:
  virtual bool intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t) = 0;
  virtual ~Primitive();
};

class Sphere : public Primitive {
public:
  virtual bool intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t);
  virtual ~Sphere();
};

class Cube : public Primitive {
public:
  virtual bool intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t);
  virtual ~Cube();
};

class NonhierSphere : public Primitive {
public:
  NonhierSphere(const Point3D& pos, double radius)
    : m_pos(pos), m_radius(radius)
  {
  }
  virtual bool intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t);
  virtual ~NonhierSphere();

private:
  Point3D m_pos;
  double m_radius;
};

class NonhierBox : public Primitive {
public:
  NonhierBox(const Point3D& pos, double size)
    : m_pos(pos), m_size(size)
  {
  }
  virtual bool intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t);
  
  virtual ~NonhierBox();

private:
  Point3D m_pos;
  double m_size;
};

#endif
