#include "scene.hpp"
#include <iostream>

SceneNode::SceneNode(const std::string& name)
  : m_name(name)
{
}

SceneNode::~SceneNode()
{
}

bool SceneNode::intersection(Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t, Colour* kd, Colour* ks, double* shininess, double* reflectivity) {
  return intersection(Matrix4x4(), rayOrigin, rayDirection, N, t, kd, ks, shininess, reflectivity);
}

bool SceneNode::intersection(Matrix4x4 matrix, Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t, Colour* kd, Colour* ks, double* shininess, double* reflectivity) {

  Vector3D N_old;
  double t_old = 0, shininess_old = 0, reflectivity_old = 0;
  Colour kd_old, ks_old;
  bool result = false;
  Matrix4x4 invMatrix = m_invtrans;

  if (is_joint()) {
    // Apply joint rotations
    double xRot = ((JointNode*)this)->get_joint_x() * M_PI / 180.0;
    if (xRot != 0) {
      invMatrix = Matrix4x4(
            Vector4D(1,        0,         0, 0),
            Vector4D(0, cos(xRot), -sin(xRot), 0),
            Vector4D(0, sin(xRot),  cos(xRot), 0),
            Vector4D(0,        0,         0, 1)
          ).invert() * invMatrix;
    }

    double yRot = ((JointNode*)this)->get_joint_y() * M_PI / 180.0;
    if (yRot != 0) {
      invMatrix = Matrix4x4(
            Vector4D( cos(yRot), 0, sin(yRot), 0),
            Vector4D(        0, 1,        0, 0),
            Vector4D(-sin(yRot), 0, cos(yRot), 0),
            Vector4D(        0, 0,        0, 1)
          ).invert() * invMatrix;
    }
  }

  for (auto& child : m_children) {
    Vector3D N_new;
    double t_new = 0, shininess_new = 0, reflectivity_new = 0;
    Colour kd_new, ks_new;
    // Do interesection on child
    if (child->intersection(invMatrix, invMatrix * rayOrigin, invMatrix * rayDirection, &N_new, &t_new, &kd_new, &ks_new, &shininess_new, &reflectivity_new)) {
      if ((t_new < t_old && t_new > 0) || !result) {
        result = true;
        // This is a closer object and the one we care about
        N_old = N_new;
        t_old = t_new;
        kd_old = kd_new;
        ks_old = ks_new;
        shininess_old = shininess_new;
        reflectivity_old = reflectivity_new;

        // Transform normal
        N_old = invMatrix.transpose() * N_old;
      }
    }
  }

  *N = N_old;
  *t = t_old;
  *kd = kd_old;
  *ks = ks_old;
  *shininess = shininess_old;
  *reflectivity = reflectivity_old;

  return result;
}

void SceneNode::rotate(char axis, double angle)
{
  Matrix4x4 m_rotation;
  double rad = angle * M_PI / 180.0;
  switch(axis) {
    case 'x':
    case 'X':
        m_rotation = Matrix4x4(
          Vector4D(1,        0,         0, 0),
          Vector4D(0, cos(rad), -sin(rad), 0),
          Vector4D(0, sin(rad),  cos(rad), 0),
          Vector4D(0,        0,         0, 1)
          );
      break;
    case 'y':
    case 'Y':
        m_rotation = Matrix4x4(
          Vector4D( cos(rad), 0, sin(rad), 0),
          Vector4D(        0, 1,        0, 0),
          Vector4D(-sin(rad), 0, cos(rad), 0),
          Vector4D(        0, 0,        0, 1)
          );
      break;
    case 'z':
    case 'Z':
        m_rotation = Matrix4x4(
          Vector4D(cos(rad), -sin(rad), 0, 0),
          Vector4D(sin(rad),  cos(rad), 0, 0),
          Vector4D(       0,         0, 1, 0),
          Vector4D(       0,         0, 0, 1)
          );
      break;
    default:
      break;
  }
  Matrix4x4 newMatrix = m_trans * m_rotation;
  set_transform(newMatrix);
}

void SceneNode::scale(const Vector3D& amount)
{
  Matrix4x4 newMatrix = m_trans * Matrix4x4(Vector4D(amount[0],         0,         0, 0),
                                            Vector4D(        0, amount[1],         0, 0),
                                            Vector4D(        0,         0, amount[2], 0),
                                            Vector4D(        0,         0,         0, 1));

  set_transform(newMatrix);
}

void SceneNode::translate(const Vector3D& amount)
{
  Matrix4x4 newMatrix = m_trans * Matrix4x4(Vector4D(1, 0, 0, amount[0]),
                                            Vector4D(0, 1, 0, amount[1]),
                                            Vector4D(0, 0, 1, amount[2]),
                                            Vector4D(0, 0, 0,         1));
  set_transform(newMatrix);
}

bool SceneNode::is_joint() const
{
  return false;
}

JointNode::JointNode(const std::string& name)
  : SceneNode(name)
{
}

JointNode::~JointNode()
{
}

bool JointNode::is_joint() const
{
  return true;
}

void JointNode::set_joint_x(double min, double init, double max)
{
  m_joint_x.min = min;
  m_joint_x.init = init;
  m_joint_x.max = max;
}

void JointNode::set_joint_y(double min, double init, double max)
{
  m_joint_y.min = min;
  m_joint_y.init = init;
  m_joint_y.max = max;
}

GeometryNode::GeometryNode(const std::string& name, Primitive* primitive)
  : SceneNode(name),
    m_primitive(primitive)
{
}

GeometryNode::~GeometryNode()
{
}

bool GeometryNode::intersection(Matrix4x4 matrix, Point3D rayOrigin, Vector3D rayDirection, Vector3D* N, double* t, Colour* kd, Colour* ks, double* shininess, double* reflectivity) {
  Matrix4x4 invTransMatrix = m_invtrans * matrix;

  bool result = m_primitive->intersection(m_invtrans * rayOrigin, m_invtrans * rayDirection, N, t);

  if (result && *t > 0.0000001) {
    *kd = m_material->get_kd();
    *ks = m_material->get_ks();
    *shininess = m_material->get_shininess();
    *reflectivity = m_material->get_reflectivity();

    // Transform normal
    *N = m_invtrans.transpose() * (*N);
  } else {
    result = false;
  }

  return result;
}
