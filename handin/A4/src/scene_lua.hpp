#ifndef SCENE_LUA_HPP
#define SCENE_LUA_HPP

#include <string>
#include "scene.hpp"

extern bool DRAW_BOUNDING_BOXES;

bool run_lua(const std::string& filename);

#endif
